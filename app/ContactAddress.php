<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContactAddress extends Model
{
    protected $fillable=['title','mobile','email','address','facebook','twiter','instragram','google','pinterest','whatsapp'];
}
