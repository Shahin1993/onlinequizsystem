<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Options extends Model
{
    protected $fillable=['questions_id','option','status'];
    public function quizes()
    {
    	return $this->belongsTo(Quizes::class);
    }
}
