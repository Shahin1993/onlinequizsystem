<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    protected $fillable=['course_image','course_name','course_title','course_description'];
}
