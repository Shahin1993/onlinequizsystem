<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Bookimages extends Model
{
	use SoftDeletes;
     protected $fillable=['books_id','image'];
}
