<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
   protected $fillable=['image','slider_title','slider_description'];
}
