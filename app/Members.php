<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Members extends Model
{
    protected $fillable=['first_name','mobile','father_name','mother_name','birth_date','address','occupation','educational_qualification','organization_name','user_email','image','facebook_link','twitter_link','google_link','instagram_link','join_date','email'];
}
