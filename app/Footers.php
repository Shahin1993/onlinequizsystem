<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footers extends Model
{
    protected $fillable=['footer'];
}
