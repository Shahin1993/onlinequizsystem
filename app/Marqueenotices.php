<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Marqueenotices extends Model
{
    protected $fillable=['title','link'];
}
