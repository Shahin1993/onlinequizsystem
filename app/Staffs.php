<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Staffs extends Model
{
	use SoftDeletes;
    protected $fillable=['offices_id','image','staff_name','degree','designation','department','institute','details'];
}
