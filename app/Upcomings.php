<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Upcomings extends Model
{
     protected $fillable=['title','image','details'];
}
