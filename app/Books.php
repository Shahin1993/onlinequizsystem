<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $fillable=['categories_id','catelog','title', 'short_description' ,'description','tag','user_id'];
    public function categories()
    {
      return $this->belongsTo('App\Categories');
    }
      public function bookimages()
    {
    	return $this->hasMany(Bookimages::class);
    }
}
