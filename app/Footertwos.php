<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footertwos extends Model
{
    protected $fillable=['title','phone','mobile','email','website_link','location'];
}
