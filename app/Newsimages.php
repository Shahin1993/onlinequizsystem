<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Newsimages extends Model
{
	use SoftDeletes;
    protected $fillable=['news_id','image'];
}
