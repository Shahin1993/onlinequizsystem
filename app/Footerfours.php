<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footerfours extends Model
{
    protected $fillable=['title','content'];
}
