<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Offices extends Model
{
	use SoftDeletes;
    protected $fillable=['office_name'];
    public function staffs()
    {
    	return $this->hasMany(Staffs::class);
    }
}
