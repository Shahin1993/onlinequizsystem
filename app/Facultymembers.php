<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facultymembers extends Model
{
    protected $fillable=['faculties_id','image','teacher_name','degree','designation','department','institute','details'];
}
