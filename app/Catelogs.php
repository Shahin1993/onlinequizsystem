<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catelogs extends Model
{
    protected $fillable=['categories_id','catelog'];
    public function categories()
    {
    	return $this->belongsTo(Categories::class);
    }
}
