<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Laravel\Socialite\Facades\Socialite;
use Cartalyst\Sentinel\Users\UserInterface;
use Sentinel;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

  public function redirectToProvider($proveider)
    {
        return Socialite::driver($proveider)->redirect();
    }

    /**
     * Obtain the user information from github.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback($proveider)
    {
        $Socialuser = Socialite::driver($proveider)->user();
        
        $check=User::where('email',$Socialuser->email)->first();
        
        if($check){
            $user=Sentinel::findById($check->id);
          Sentinel::loginAndRemember($user);
            return redirect('/MyExamResult');
               
        }else{
            $data =[
            'first_name'=> $Socialuser->name,
            'email'=> $Socialuser->email,
            'password'=> bcrypt('12ABC321'),
            ];
        
        
        $user=Sentinel::registerAndActivate($data);
        $role= Sentinel::findRoleByslug('member');
        $role->users()->attach($user);
        Sentinel::authenticate($user);

        return redirect('/MyExamResult');
        }
        
        
    }
    


}
