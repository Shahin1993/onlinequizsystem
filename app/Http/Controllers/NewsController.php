<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Newsimages;
use App\SoftDeletes;
use Image;
use File;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $newsad=News::orderBy('id','desc')->paginate(20);
        return view('admin.news.view',compact('newsad'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $this->validate($request,[
            'title'=>'required',
             'description'=>'max:5000',
             'featured_image'=>'image|mimes:jpeg,png,jpg,JPG,PNG,gif,svg|max:11048',   
         ]);
         $data=new News;
        if($request->hasfile('featured_image')){
            $image=$request->file('featured_image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(300,260);
        $image_resize->save('images/news_featuredimages/'.$file_name);
       
        $data->featured_image=$file_name;
         }
        $data->title=$request->title;
        $data->description=$request->description;   
        $data->date=$request->date;
        $data->save();
    if ($request->hasfile('image')){
     if(count($request->image) > 0) {
    foreach ($request->image as $item=>$images) {
        $file_name=rand().'.'.$images->getClientOriginalExtension();
        $image_resize=Image::make($images->getRealPath());
       // $image_resize->resize(800,600);
        $image_resize->resize(null, 600, function ($constraint) {
    $constraint->aspectRatio();
});
        $image_resize->save('images/news_images/'.$file_name);
        $datae=new Newsimages;
         $datae->news_id= $data->id;
        $datae->image=$file_name;   
        $datae->save();
  }
    }
    }
    return redirect()->back()->with('success','Data inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=News::find($id);
        return view('admin.news.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=News::find($id);
        return view('admin.news.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
             'description'=>'max:5000',
             'featured_image'=>'image|mimes:jpeg,png,jpg,JPG,PNG,gif,svg|max:11048',   
         ]);
         $data=News::find($id);
        if($request->hasfile('featured_image')){
            $image=$request->file('featured_image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->featured_image;
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(400,260);
        $image_resize->save('images/news_featuredimages/'.$file_name);
        $data->featured_image=$file_name;
        $image_path=("images/news_featuredimages/$old_file");
        unlink( $image_path);
         }
        $data->title=$request->title;
        $data->description=$request->description;   
        $data->date=$request->date;   
        $data->save();
    if ($request->hasfile('image')){
     if(count($request->image) > 0) {
    foreach ($request->image as $item=>$images) {
        $file_name=rand().'.'.$images->getClientOriginalExtension();
        $image_resize=Image::make($images->getRealPath());
       $image_resize->resize(800,600);
        $image_resize->save('images/news_images/'.$file_name);
        $datae=new Newsimages;
        $datae->news_id= $data->id;
        $datae->image=$file_name;   
        $datae->save();
  }
    }
    }
    return redirect()->back()->with('success','Data update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=News::find($id);
        $data->newsimages()->delete();
        $data->delete();
        return redirect()->back()->with('success','Successfully Deleted this data');
    }
    public function deleteData()
    {
        $deletedNews=News::onlyTrashed()->get();
        return view('admin.news.deleted',compact('deletedNews'));
    }
     public function restore($id)
    {
        $data=Newsimages::onlyTrashed()->where('news_id','=',$id)->restore();
        $data=News::onlyTrashed()->find($id)->restore();
        return redirect('/newses')->with('success','Succesfully restore this data'); 
    }
public function permanentDelete($id)
    {
        $data=News::onlyTrashed()->find($id);
        $eimage=Newsimages::onlyTrashed()->where('news_id','=',$id)->get();
foreach($eimage as $image){
   $image_path = public_path().'/images/news_images/'.$image->image;
   if(File::exists($image_path)) {
    File::delete($image_path);
     }
}
        unlink("images/news_featuredimages/$data->featured_image");
         $data->newsimages()->forceDelete();
         $data->forceDelete();
        return redirect()->back()->with('success','Successfully Deleted this data');
    }
}
