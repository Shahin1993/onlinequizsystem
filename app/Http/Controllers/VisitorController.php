<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Notices;
use App\Catelogs;
class VisitorController extends Controller
{
    public function noticeView($id)
    {
    	$data=Notices::find($id);
    	if($data==""){
          
          return('দুঃখিত!!আপনি যা খুজতেছেন তা পাওয়া যায়নি!!
            <br>Sorry!!Not found your search!! ');
         }
         else{
         	return view('front_page.notice.details',compact('data'));
         }
    	
    }

    public function catelog()
    {
      $categories_id = Input::get('categories_id');
      $catelog = Catelogs::orderBy('catelog','asc')->where('categories_id', '=', $categories_id)->get();
      return response()->json($catelog);
    }
}
