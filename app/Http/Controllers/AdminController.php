<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\Role_users;
class AdminController extends Controller
{
	public function __construct()
    {
      
        $this->middleware('manager');
    }
    public function index()
    {
    	return view('admin.dashboard');
    	
    }
    public function user()
    {
        $user=User::paginate(30);
        return view('admin.user.user_list',compact('user'));
    }
     public function manageUser($id)
    {
            $users=User::find($id);
    	return view('admin.user.manage_user',compact('users'));
    }
    public function userAccess(request $request, $id)
    {
    	$user=Role_users::where('user_id','=',$id);
    	 $data=$request->all();
         $user->update($data);
         return redirect('/user')->with('success','Role has been changed');
    }
    public function destroy($id)
    {
    	$data=User::find($id);
    	$data->delete();
    	return redirect()->back()->with('success','Deleted This User'); 
    }
    public function passwordcreate()
    {
        return view('backend.newpassword.create');
    }
    public function resetpassword(Request $request)
    {
         $request->validate([
            'student_id'=>'required',
            'password'=>'required|min:4',
            'confirm_password'=>'required_with:password|same:password|min:4',
            
        ]);
       $password=$request->password;
       $confirm_password=$request->confirm_password;
       $sentinelUser=User::whereEmail($request->student_id)->first();
        if($sentinelUser==""){
          return redirect()->back()->with('warning','User id Could not found');
}else{
   
     $user=Sentinel::findById($sentinelUser->id);
        
            if ($password != $confirm_password) {
            return redirect()->back()->with('warning','New password and confirm password not match');
        }else{
        Sentinel::update($user,array('password'=>$password
        ));
        return back()->with('success','password successfully reset');
    }
}
        

    }
}
