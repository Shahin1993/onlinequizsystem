<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscribers;
class SubscriberController extends Controller
{
	public function __construct()
    {
      
        $this->middleware('manager');
    }
    public function index()
    {
    	$subscribers=Subscribers::orderBy('id','desc')->paginate(30);
        return view('admin.subscriber.view',compact('subscribers'));
    }
}
