<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services;
class ServiceController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $service=Services::orderBy('id','desc')->get();
        return view('admin.service.view',compact('service'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $service=Services::orderBy('id','desc')->get();
        return view('admin.service.create',compact('service'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'description'=>'required|max:5000000',
            'short_description'=>'required|max:800',
        ]);
        $data=$request->all();
        Services::create($data);
        return redirect()->back()->with('success','Data add successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Services::find($id);
        return view('admin.service.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $request->validate([
            'description'=>'required|max:5000000',
            'short_description'=>'required|max:800',
        ]);
      $data=Services::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect('/services')->with('success','Data update successfully');   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Services::find($id);
        $data->delete();
        return redirect()->back()->with('success','Data deleted successfully');   
    }
}
