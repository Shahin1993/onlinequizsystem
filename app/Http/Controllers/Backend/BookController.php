<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Books;
use App\Bookimages;
use Image;
class BookController extends Controller
{
     public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books=Books::orderBy('id','desc')->paginate(30);
        return view('backend.book.view',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $this->validate($request,[
        'categories_id'=>'required',
        'catelog'=>'required',
        'title'=>'required',
        'description'=>'required',
        'tag'=>'required',


       ]);
       $data=$request->all();
      $did= Books::create($data)->id;
       
        if ($request->hasfile('image')){
     if(count($request->image) > 0) {
    foreach ($request->image as $item=>$images) {
        $file_name=rand().'.'.$images->getClientOriginalExtension();
        $image_resize=Image::make($images->getRealPath());
       $image_resize->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
});
        $image_resize->save('images/book_images/'.$file_name);
        $datae=new Bookimages;
         $datae->books_id= $did;
        $datae->image=$file_name;   
        $datae->save();
  }
    }
    }
     
       return redirect()->back()->with('success','Data Add Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Books::find($id);
        return view('backend.book.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Books::find($id);
        return view('backend.book.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       $this->validate($request,[
            'categories_id'=>'required',
        'catelog'=>'required',
        'title'=>'required',
        'description'=>'required',
        'tag'=>'required',
        ]);
        $category=Books::find($id);
        $data=$request->all();
        $category->update($data);
        
          if ($request->hasfile('image')){
     if(count($request->image) > 0) {
    foreach ($request->image as $item=>$images) {
        $file_name=rand().'.'.$images->getClientOriginalExtension();
        $image_resize=Image::make($images->getRealPath());
       $image_resize->resize(800, null, function ($constraint) {
    $constraint->aspectRatio();
});
        $image_resize->save('images/book_images/'.$file_name);
        $datae=new Bookimages;
         $datae->books_id= $id;
        $datae->image=$file_name;   
        $datae->save();
  }
    }
    }
     
        
        return redirect()->back()->with('success','Update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Books::find($id);
         $data->bookimages()->delete();
        $data->delete();
        return redirect()->back()->with('success','delete this data');
    }
    public function DeleteBookImage($id)
    {
         $data=Bookimages::find($id);
        $data->delete();
        return redirect()->back()->with('success','delete this data');
    }
}
