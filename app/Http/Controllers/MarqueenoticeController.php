<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Marqueenotices;
class MarqueenoticeController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mdata=Marqueenotices::orderBy('id','desc')->paginate(30);
        return view('admin.marquee_notice.view',compact('mdata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.marquee_notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'title'=>'required',
        ]);
        $data=$request->all();
        Marqueenotices::create($data);
        return redirect()->back()->with('success','Data add successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Marqueenotices::find($id);
        return view('admin.marquee_notice.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Marqueenotices::find($id);
        return view('admin.marquee_notice.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
        ]);
        $data=Marqueenotices::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect('/mnotices')->with('success','Successfully Data Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Marqueenotices::find($id);
        $data->delete();
        return redirect()->back()->with('success','Delete this data successful');
    }
}
