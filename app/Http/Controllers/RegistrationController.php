<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Sentinel;
use Activation;
use Mail;
class RegistrationController extends Controller
{
    public function signup()
    {
    	return view('authentication.signup');
    }
    public function store(Request $request)
    {
    $request->validate([
    'first_name' => 'required|max:120',
   
    'email' =>  'required|unique:users',
    
    'password' => 'required|min:4',
    'confirm_password' => 'required_with:password|same:password|min:4',
]);
      $user=Sentinel::registerAndActivate($request->all());
      
      $role =Sentinel::findRoleBySlug('member');
      $role->users()->attach($user);
      
      return redirect()->back()->with('success','Registration successful');
       
    }
    private function sendEmail($user,$code)
    {
     Mail::send('emails.activation',[
        'user'=>$user,
        'code'=>$code
     ],function($message) use ($user){
      $message->to($user->email);
      $message->subject("Hello $user->first_name Active your account");
     });
    }
    
}
