<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Sentinel;
use Reminder;
use Mail;
class ForgottpasswordController extends Controller
{
    public function create()
    {
    	return view('authentication.forgotpassword');
    }
    public function postemail(Request $request)
    {
    	$userID=User::whereEmail($request->email)->get();
    	if($userID->count()==0)
    		return redirect()->back()->with('error','email not found');
    	else
    	$user=User::whereEmail($request->email)->first();
    	$sentinelUser=Sentinel::findById($user->id);
        $reminder=Reminder::exists($sentinelUser)?:Reminder::create($sentinelUser);
        $this->sendEmail($user,$reminder->code);
        return redirect()->back()->with('success','Reset code send your email, Please check your email');
    }
    private function sendEmail($user,$code)
    {
      Mail::send('emails.resetpassword',[
        'user'=>$user,
        'code'=>$code,
     ],function($message) use ($user){
      $message->to($user->email);
      $message->subject("Hello $user->first_name Reset your password");

     });
    }
    public function resetpassword($email,$resetCode)
    {
    	$user=User::byEmail($email);
    	if ($user->count()==0) {
    		abort(404);
    	}
    	$sentinetUser=Sentinel::findById($user->id);
    	if ($reminder=Reminder::exists($sentinetUser)) {
    		if ($resetCode=$reminder->code) {
    			return view('authentication.resetpassword_create');
    		}
    		else{
    			return redirect('/');
    		}
    	}
    	else{
    		return redirect('/');
    	}
    }

public function PostResetPassword(Request $request, $email, $resetCode)
    {
    	$this->validate($request,[
        'password'=>'required|min:6',
        'confirm_password'=>'required_with:password|same:password|min:6',
        ]);
       $user=User::byEmail($email);
       if(count($user)==0) {
    		abort(404);
    	}
    	$sentinetUser=Sentinel::findById($user->id);
    	if ($reminder=Reminder::exists($sentinetUser)) {
    		if ($resetCode==$reminder->code) {
              Reminder::complete($sentinetUser,$resetCode,$request->password);
    			return redirect('/login')->with('success','Password reset successfully');
    		}
    		else{
    			return redirect('/');
    		}
    	}
    	else{
    		return redirect('/');
    	}
    }
}
