<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Sentinel;
use Mail;
class UserController extends Controller
{
    public function signup()
    {
    	return view('backend.new_user.create');
    }
    public function store(Request $request)
    {
    $request->validate([
    'first_name' => 'required|max:70',
    'email' =>  'required|unique:users',
    'mobile' => 'nullable',
    'password' => 'required|min:4',
    'confirm_password' => 'required_with:password|same:password|min:4',
]);
      $user=Sentinel::registerAndActivate($request->all());
      // $activation=Activation::create($user);
      $role =Sentinel::findRoleBySlug($request->slug);
      $role->users()->attach($user);
      // $this->sendEmail($user);
      return redirect()->back()->with('success','Registration successful');
       
    }
    private function sendEmail($user,$code)
    {
     Mail::send('emails.account_information',[
        'user'=>$user,
     ],function($message) use ($user){
      $message->to($user->user_email);
      $message->subject("Hello $user->name Please Check This email ");
     });
    }
}
