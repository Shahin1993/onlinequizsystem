<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Image;
use App\Facultymembers;
use App\Faculties;
class FacultyMemberController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    public function index()
    {
        
        return view('admin.facultymembers.view');
    }
    public function create()
    {
      return view('admin.facultymembers.create');
    }
    public function store(Request $request)
    {
        $request->validate([
        	'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'teacher_name'=>'required|max:90',
        	'degree'=>'required|max:90',
        	'designation'=>'required|max:70',
        	'department'=>'required|max:70',
        ]);
        if($request->hasfile('file')){
        	$image=$request->file('file');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(270,253);
        $image_resize->save('images/teacher_images/'.$file_name);
        $data=new Facultymembers;
        $data->faculties_id=$request->faculties_id;
        $data->image=$file_name;
        $data->teacher_name=$request->teacher_name;
        $data->degree=$request->degree;
       $data->designation=$request->designation;      
       $data->department=$request->department;      
        $data->institute=$request->institute;      
        $data->details=$request->details;      
        $data->save();
        return redirect()->back()->with('success','Data inserted successfully');
        }
         else{
         	$data=$request->all();
        Facultymembers::create($data);
        return redirect()->back()->with('success','Data inserted successfully');
         }
        
    }
    public function edit($id)
    {
           $data=Facultymembers::find($id);
           return view('admin.facultymembers.edit',compact('data'));
    }
    public function update(Request $request,$id)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'teacher_name'=>'required|max:90',
            'degree'=>'required|max:90',
            'designation'=>'required|max:70',
            'department'=>'required|max:70',
        ]);
        $data=Facultymembers::find($id);
        if($request->hasfile('file')){
            $image=$request->file('file');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->image;
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(270,253);
        $image_resize->save('images/teacher_images/'.$file_name);
        $image_path=("images/teacher_images/$old_file");
        if($old_file!='default_faculty.jpg'){
        unlink( $image_path);
        }
        $data->image=$file_name;
         }
        
        $data->faculties_id=$request->faculties_id;
        
        $data->teacher_name=$request->teacher_name;
        $data->degree=$request->degree;
       $data->designation=$request->designation;      
       $data->department=$request->department;      
        $data->institute=$request->institute;      
        $data->details=$request->details;      
        $data->save();
        return redirect()->back()->with('success','Data Update successfully');
       
    }
    public function show($id)
    {
       $data=Facultymembers::find($id);
           return view('admin.facultymembers.details',compact('data'));
    }
    public function destroy($id)
    {
        $data=Facultymembers::find($id);
        if($data->image=='default_faculty.jpg'){
        $data->delete();
           return redirect()->back()->with('success','Successfully Delete This Data');
        }else{
             unlink("images/teacher_images/$data->image");  
             $data->delete();
             return redirect()->back()->with('success','Successfully Delete This Data');
        }
    }
    public function fmembers()
    {
      $facultie_id =Input::get('faculties_id');
      $fmembers=Facultymembers::where('faculties_id', '=', $facultie_id)->get();
      return response()->json($fmembers);
    }
}
