<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Events;
use App\Eventimages;
use App\SoftDeletes;
use Image;
use File;
class EventController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventsad=Events::orderBy('id','desc')->paginate(20);
        return view('admin.event.view',compact('eventsad'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.event.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
             'description'=>'max:5000',
             'featured_image'=>'image|mimes:jpeg,png,jpg,JPG,PNG,gif,svg|max:11048',   
         ]);
         $data=new Events;
        if($request->hasfile('featured_image')){
            $image=$request->file('featured_image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(400,260);
        $image_resize->save('images/event_featuredimages/'.$file_name);
       
        $data->featured_image=$file_name;
         }
        $data->title=$request->title;
        $data->short_description=$request->short_description;   
        $data->description=$request->description;   
        $data->date=$request->date;   
        $data->location=$request->location;   
        $data->save();
    if ($request->hasfile('image')){
     if(count($request->image) > 0) {
    foreach ($request->image as $item=>$images) {
        $file_name=rand().'.'.$images->getClientOriginalExtension();
        $image_resize=Image::make($images->getRealPath());
       $image_resize->resize(null, 600, function ($constraint) {
    $constraint->aspectRatio();
});
        $image_resize->save('images/event_images/'.$file_name);
        $datae=new Eventimages;
         $datae->events_id= $data->id;
        $datae->image=$file_name;   
        $datae->save();
  }
    }
    }
    return redirect()->back()->with('success','Data inserted successfully');
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Events::find($id);
        return view('admin.event.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $data=Events::find($id);
        return view('admin.event.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $this->validate($request,[
            'title'=>'required',
             'description'=>'max:5000',
             'featured_image'=>'image|mimes:jpeg,png,jpg,JPG,PNG,gif,svg|max:11048',   
         ]);
         $data=Events::find($id);
        if($request->hasfile('featured_image')){
            $image=$request->file('featured_image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->featured_image;
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(400,260);
        $image_resize->save('images/event_featuredimages/'.$file_name);
        $data->featured_image=$file_name;
        $image_path=("images/event_featuredimages/$old_file");
        unlink( $image_path);
         }
        $data->title=$request->title;
        $data->short_description=$request->short_description;   
        $data->description=$request->description;   
        $data->date=$request->date;   
        $data->location=$request->location;   
        $data->save();
    if ($request->hasfile('image')){
     if(count($request->image) > 0) {
    foreach ($request->image as $item=>$images) {
        $file_name=rand().'.'.$images->getClientOriginalExtension();
        $image_resize=Image::make($images->getRealPath());
       $image_resize->resize(null, 600, function ($constraint) {
    $constraint->aspectRatio();
});
        $image_resize->save('images/event_images/'.$file_name);
        $datae=new Eventimages;
         $datae->events_id= $data->id;
        $datae->image=$file_name;   
        $datae->save();
  }
    }
    }
    return redirect('/events')->with('success','Data update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Events::find($id);
        $data->eventimages()->delete();
        $data->delete();
        return redirect()->back()->with('success','Successfully Deleted this data');
    }
    public function deleteData()
    {
        $deletedEvent=Events::onlyTrashed()->get();
        return view('admin.event.deleted',compact('deletedEvent'));
    }
    public function restore($id)
    {
        $data=Eventimages::onlyTrashed()->where('events_id','=',$id)->restore();
        $data=Events::onlyTrashed()->find($id)->restore();
        return redirect('/events')->with('success','Succesfully restore this data'); 
    }
    public function permanentDelete($id)
    {
        $data=Events::onlyTrashed()->find($id);
        $image_pathf = public_path().'/images/event_featuredimages/'.$data->featured_image;
   if(File::exists($image_pathf)) {
    File::delete($image_pathf);
     }
        $eimage=Eventimages::onlyTrashed()->where('events_id','=',$id)->get();
foreach($eimage as $image){
    $image_path = public_path().'/images/event_images/'.$image->image;
   if(File::exists($image_path)) {
    File::delete($image_path);
     }
     }
        $data->eventimages()->forceDelete();
        $data->forceDelete();
        return redirect()->back()->with('success','Successfully Deleted this data');
    }
    
    
}
