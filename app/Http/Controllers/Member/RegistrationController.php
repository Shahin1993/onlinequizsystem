<?php

namespace App\Http\Controllers\Member;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Members;
use Sentinel;
use Image;
class RegistrationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $etsmember=Members::paginate(50);
        return view('front_page.member.view',compact('etsmember'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('front_page.member.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
    'first_name' => 'required|max:100',
    'email' =>  'required|unique:users',
    'mobile' => 'required',
    'password' => 'required|min:4',
    'confirm_password' => 'required_with:password|same:password|min:4',
]);
      $user=Sentinel::registerAndActivate($request->all());
      $role =Sentinel::findRoleBySlug('member');
      $role->users()->attach($user);

     $member=new Members;
      if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=rand().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(300,300);
        $image_resize->save('images/member_images/'.$file_name);
         $member->image=$file_name;
            }
            $member->user_id=$user->id;
            $member->first_name=$request->first_name;
            $member->mobile=$request->mobile;
            $member->father_name=$request->father_name;
            $member->mother_name=$request->mother_name;
            $member->birth_date=$request->birth_date;
            $member->address=$request->address;
            $member->occupation=$request->occupation;
            $member->education=$request->education;
            $member->organization_name=$request->organization_name;
            $member->join_date=$request->join_date;
            $member->user_email=$request->user_email;
            $member->email=$request->email;
            $member->facebook_link=$request->facebook_link;
            $member->twitter_link=$request->twitter_link;
            $member->google_link=$request->google_link;
            $member->instagram_link=$request->instagram_link;
            $member->save();
            return back()->with('success','Thanks for join us');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
