<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Educations;
class EducationController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.education.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.education.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'description'=>'required|max:5000000',
            'short_description'=>'required|max:800',
        ]);
        $data=$request->all();
        Educations::create($data);
        return redirect()->back()->with('success','Data add successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Educations::find($id);
        return view('admin.education.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Educations::find($id);
        return view('admin.education.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'description'=>'required|max:5000000',
            'short_description'=>'required|max:800',
        ]);
        $data=Educations::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect('/educations')->with('success','Successfully Data Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Educations::find($id);
        $data->delete();
        return redirect()->back()->with('success','Successfully delete this data');
    }
}
