<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ContactAddress;
class ContactAddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  $contact_address=ContactAddress::all();
        return view('admin.contact_address.view',compact('contact_address'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.contact_address.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'mobile'=>'required|max:80',
            'email'=>'required',
            'address'=>'required',
        ]);
        $data=$request->all();
        ContactAddress::create($data);
        return redirect()->back()->with('success','Contact Address Submit Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=ContactAddress::find($id);
        return view('admin.contact_address.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=ContactAddress::find($id);
        return view('admin.contact_address.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'mobile'=>'required|max:80',
            'email'=>'required',
            'address'=>'required',
        ]);
        $data=ContactAddress::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect()->back()->with('success','Data updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=ContactAddress::find($id);
        $data->delete();
        return redirect()->back()->with('success','Deleted this data successfully');
    }
}
