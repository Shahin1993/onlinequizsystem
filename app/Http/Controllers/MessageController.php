<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Messages;
use Image;
class MessageController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.home_message.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.home_message.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'name'=>'required|max:90',
            'description'=>'required|max:5000000',
            
        ]);
        if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(500,700);
        $image_resize->save('images/message_images/'.$file_name);
        $data=new Messages;
        $data->image=$file_name;
        $data->name=$request->name;
        $data->designation=$request->designation;
        $data->short_description=$request->short_description;   
        $data->description=$request->description;   
        $data->save();
        return redirect()->back()->with('success','Data inserted successfully');
        }
         else{
            $data=$request->all();
        Messages::create($data);
        return redirect()->back()->with('success','Data inserted successfully');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Messages::find($id);
        return view('admin.home_message.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

         $data=Messages::find($id);
        return view('admin.home_message.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
       
       $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'name'=>'required|max:90',
            'description'=>'required|max:5000000',
            
        ]);
         $data=Messages::find($id);

    if($request->hasfile('image'))
     {
        $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->image;
         $image_resize = Image::make($image->getRealPath());              
       $image_resize->resize(500,700);
        $image_resize->save('images/message_images/'.$file_name);
        $data->image= $file_name;
        if($old_file !=""){
            $image_path=("images/message_images/$old_file");
        unlink( $image_path);
        }
        

    }
        $data->name=$request->name;
        $data->designation=$request->designation;
        $data->short_description=$request->short_description;   
        $data->description=$request->description;   
        $data->save();
        return redirect()->back()->with('success','Data Update successfully');
        
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Messages::find($id);
        if($data->image!=""){
        unlink("images/message_images/$data->image");
      }
        $data->delete();
        return redirect()->back()->with('success','Successfully Deleted This Data');
    }
}
