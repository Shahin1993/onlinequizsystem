<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;

use Sentinel;
class LoginController extends Controller
{
    public function login()
    {
    	return view('frontend.login.create');
    }
    public function postLogin(Request $request)
    {

    	try{
            $rememberMe=true;
            $user = Sentinel::authenticate($request->all(),$rememberMe);
         if($user=Sentinel::check()){

             $user_type=Sentinel::getUser()->roles()->first()->slug;
             if($user_type=="admin")
         return redirect('/infra');
     elseif($user_type=="manager")
        return redirect('/infra');
    elseif($user_type=="visitor")
        return redirect('/');
    elseif($user_type=="member")
        return redirect('/profile');
     }else{
        return redirect()->back()->with('error','Oops!! Email or Password Not Matching');
     }
        }catch(ThrottlingException $e){
        $delay = $e->getDelay();
        $min=floor($delay/60);
        return redirect()->back()->with(['error'=>"Your are suspended for $min minute"]); 
        }
        catch(NotActivatedException $e){
        return redirect()->back()->with(['error'=>"Your account is not Activate"]); 
        }       	     
    }
public function logout()
{
    Sentinel::logout();
    return redirect('/login');
}
     
}
