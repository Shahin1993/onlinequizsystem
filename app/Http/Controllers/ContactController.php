<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacts;
use App\ContactAddress;
use Mail;
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts=Contacts::orderBy('id','desc')->get();
        return view('admin.contact.view',compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $contactAddress=ContactAddress::orderBy('id','desc')->get();
    return view('frontend.contact.create',compact('contactAddress'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'name'=>'required|max:80',
            'email'=>'required',
            'phone'=>'required|max:13',
            'subject'=>'required|max:80',
            'details'=>'required', 
        ]);
        $data=$request->all();
        $contact=Contacts::create($data);
        //$this->sendEmail($contact);
        return redirect()->back()->with('success','Message Sent Successfully');
    }
    private function sendEmail($contact)
    {
     Mail::send('emails.contact',[
        'contact'=>$contact,
     ],function($message) use ($contact){
      $message->to('chf.shahin@gmail.com');
      $message->subject("$contact->subject");
     });
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Contacts::find($id);
        return view('admin.contact.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'=>'required|max:80',
            'email'=>'required',
            'mobile'=>'required|max:13',
            'subject'=>'required|max:80',
            'details'=>'required',
            
        ]);
        $data=Contacts::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect('/contacts')->with('success','Successfully Data Update');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Contacts::find($id);
        $data->delete();
        return redirect()->back()->with('success','Delete this data Succesfully');
    }
}
