<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Coursefees;
use Image;
class CoursefeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fees=Coursefees::all();
         return view('admin.fess.view',compact('fees'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('admin.fess.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'title'=>'required|max:250',
            'details'=>'required|max:5000000',  
        ]);
        if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1280,1656);
        $image_resize->save('images/notice_images/'.$file_name);
        $data=new Coursefees;
        $data->image=$file_name;
        $data->title=$request->title;
        $data->details=$request->details;   
        $data->save();
        return redirect()->back()->with('success','Data inserted successfully');
        }
         else{
            $data=$request->all();
        Coursefees::create($data);
        return redirect()->back()->with('success','Data inserted successfully');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Coursefees::find($id);
        return view('admin.fess.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Coursefees::find($id);
        return view('admin.fess.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
