<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Courses;
use Image;
class CourseController extends Controller
{

    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coursesd=Courses::paginate(12);
        return view('admin.courses.view',compact('coursesd'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'course_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'course_name'=>'required|unique:courses|max:90',
            'course_title'=>'required|max:90',
            'course_description'=>'required|max:5000000',
            
        ]);
        if($request->hasfile('course_image')){
            $image=$request->file('course_image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1280,600);
        $image_resize->save('images/course_images/'.$file_name);
        $data=new Courses;
        $data->course_image=$file_name;
        $data->course_name=$request->course_name;
        $data->course_title=$request->course_title;
        $data->course_description=$request->course_description;   
        $data->save();
        return redirect()->back()->with('success','Data inserted successfully');
        }
         else{
            $data=$request->all();
        Courses::create($data);
        return redirect()->back()->with('success','Data inserted successfully');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Courses::find($id);
        return view('admin.courses.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Courses::find($id);
        return view('admin.courses.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'course_image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'course_name'=>'required|max:90',
            'course_title'=>'required|max:90',
            'course_description'=>'required|max:5000000',
            
        ]);
        $data=Courses::find($id);
        if($request->hasfile('course_image')){
            $image=$request->file('course_image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->course_image;
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1280,600);
        $image_resize->save('images/course_images/'.$file_name);
        $data->course_image=$file_name;
        $image_path=("images/course_images/$old_file");
        unlink( $image_path);
      }

        $data->course_name=$request->course_name;
        $data->course_title=$request->course_title;
        $data->course_description=$request->course_description;   
        $data->save();
        return redirect('/courses')->with('success','Data Update successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course=Courses::find($id);

        if($course->course_image==""){
         $course->delete();
        return redirect()->back()->with('success','Successfully Deleted This Data');
        }
        else{
            unlink("images/course_images/$course->course_image");
        $course->delete();
        return redirect()->back()->with('success','Successfully Deleted This Data');
        }
        
    }
}
