<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Offices;
use App\SoftDeletes;
use App\Staffs;
class OfficeController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.office.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.office.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'office_name'=>'required|unique:offices',
        ]);
        $data=$request->all();
        Offices::create($data);
        return redirect()->back()->with('success','Data add succrssfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Offices::find($id);
        return view('admin.office.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'office_name'=>'required|unique:offices',
        ]);
        $data=Offices::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return redirect('/offices')->with('success','Data Update succrssful');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Offices::find($id);
        $data->staffs()->delete();
         $data->delete();
         return redirect()->back()->with('success','Successfully Delete This');
    }
    public function restore($id)
    {
        $data=Staffs::onlyTrashed()->where('offices_id','=',$id)->restore();
        $data=Offices::onlyTrashed()->find($id)->restore();
        return redirect('/offices')->with('success','Succesfully restore this data');
        
    }
    public function deleteData()
    {
        $officeD=Offices::onlyTrashed()->get();
        return view('admin.office.deleted',compact('officeD'));
    }
    public function permanentDelete($id)
    {
        $data=Offices::onlyTrashed()->find($id);
        $data->staffs()->forceDelete();
         $data->forceDelete();
         return redirect()->back()->with('success','Successfully Delete This');
    }
}
