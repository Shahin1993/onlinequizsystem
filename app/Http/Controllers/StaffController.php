<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Staffs;
use Image;
class StaffController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.staffs.view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.staffs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'staff_name'=>'required|max:90',
            'degree'=>'required|max:90',
            'designation'=>'required|max:70',
            
        ]);
        if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(270,253);
        $image_resize->save('images/staff_images/'.$file_name);
        $data=new Staffs;
        $data->offices_id=$request->offices_id;
        $data->image=$file_name;
        $data->staff_name=$request->staff_name;
        $data->degree=$request->degree;
        $data->designation=$request->designation;
        $data->institute=$request->institute;      
        $data->details=$request->details;      
        $data->save();
        return redirect()->back()->with('success','Data inserted successfully');
        }
         else{
            $data=$request->all();
        Staffs::create($data);
        return redirect()->back()->with('success','Data inserted successfully');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data=Staffs::find($id);
           return view('admin.staffs.details',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Staffs::find($id);
           return view('admin.staffs.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'staff_name'=>'required|max:90',
            'degree'=>'required|max:90',
            'designation'=>'required|max:70',
            
        ]);
        $data=Staffs::find($id);
        if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
         $old_file=$data->image;
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(270,253);
        $image_resize->save('images/staff_images/'.$file_name);
        $image_path=("images/staff_images/$old_file");
        if($old_file!='default_faculty.jpg'){
        unlink( $image_path);
        }
        $data->image=$file_name;
        }
        $data->offices_id=$request->offices_id;
        $data->staff_name=$request->staff_name;
        $data->degree=$request->degree;
        $data->designation=$request->designation;
        $data->institute=$request->institute;      
        $data->details=$request->details;      
        $data->save();
        return redirect('/staffs')->with('success','Data Update successful');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Staffs::find($id);
        if($data->image=='default_faculty.jpg'){
        $data->delete();
           return redirect()->back()->with('success','Successfully Delete This Data');
        }else{
             unlink("images/staff_images/$data->image");  
             $data->delete();
             return redirect()->back()->with('success','Successfully Delete This Data');
        }
       
    }
     public function staffs()
    {
      $offices_id =Input::get('offices_id');
      $staffs=Staffs::where('offices_id', '=', $offices_id)->get();
      return response()->json($staffs);
    }
}
