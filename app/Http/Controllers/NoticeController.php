<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notices;
use Image;
class NoticeController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $notice_view=Notices::orderBy('id','desc')->paginate(30);
        return view('admin.notice.view',compact('notice_view'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.notice.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'title'=>'required|max:250',
            'details'=>'required|max:5000000',  
        ]);
        if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1280,1656);
        $image_resize->save('images/notice_images/'.$file_name);
        $data=new Notices;
        $data->image=$file_name;
        $data->title=$request->title;
        $data->details=$request->details;   
        $data->save();
        return redirect()->back()->with('success','Data inserted successfully');
        }
         else{
            $data=$request->all();
        Notices::create($data);
        return redirect()->back()->with('success','Data inserted successfully');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Notices::find($id);
        return view('admin.notice.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'title'=>'required|max:250',
            'details'=>'required|max:5000000',  
        ]);
        $data=Notices::find($id);
        if($request->hasfile('image')){
            $image=$request->file('image');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->image;
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1280,1656);
        $image_resize->save('images/notice_images/'.$file_name);
        $image_path=("images/notice_images/$old_file");
        unlink( $image_path);
        $data->image=$file_name;
        }
        $data->title=$request->title;
        $data->details=$request->details;   
        $data->save();
        return redirect()->back()->with('success','Data Update successfully');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Notices::find($id);
        if($data->image==""){
           $data->delete();
        return redirect()->back()->with('success','Delete this Data Successful');
        }
        else{
         unlink("images/notice_images/$data->image"); 
           $data->delete();
        return redirect()->back()->with('success','Delete this Data Successful');
    }
    }
}
