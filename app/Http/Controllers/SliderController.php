<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sliders;
USE Image;
use Session;
class SliderController extends Controller
{
    public function __construct()
    {
      
        $this->middleware('manager');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('admin.slider.slider_view');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.slider.slider_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         if($request->hasfile('file'))
     {
        $image=$request->file('file');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $image_resize = Image::make($image->getRealPath());              
        $image_resize->resize(1350,500);
        $image_resize->save('images/slider_images/'.$file_name);
        $data=new Sliders;
        $data->image=$file_name;
        $data->slider_title=$request->slider_title;
        $data->slider_description=$request->slider_description;      
        $data->save();
        return redirect()->back()->with('success','Slider Add Successfully');
     }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $slider=Sliders::find($id);
        return view('admin.slider.slider_edit',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $data=Sliders::find($id);
        if($request->hasfile('file'))
     {
        $image=$request->file('file');
        $file_name=time().'.'.$image->getClientOriginalExtension();
        $old_file=$data->image;
         $image_resize = Image::make($image->getRealPath());              
       $image_resize->resize(1350,500);
        $image_resize->save('images/slider_images/'.$file_name);
        $data->image= $file_name;
        $image_path=("images/slider_images/$old_file");
        unlink( $image_path);

    }
     $data->slider_title=$request->slider_title;
     $data->slider_description=$request->slider_description;
    $data->save();
    Session::flash('success','Update Successful');
    return redirect('/sliders');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
          $slider=Sliders::find($id);
        unlink("images/slider_images/$slider->image");
        $slider->delete();
        return redirect('/sliders')->with('success','Successfully Deleted This Data');
    }
}
