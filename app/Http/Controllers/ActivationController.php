<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Sentinel;
use Activation;
use App\Activations;
class ActivationController extends Controller
{
    public function activate($email,$activationcode)
    {
    	$user=User::whereEmail($email)->first();
    	$sentineluser=Sentinel::findById($user->id);
    	if (Activation::complete($sentineluser,$activationcode)) {
    		return redirect('/login');
    	}
    	else{

    	}
    }
    public function useractive($id)
    {
         $user  =Activations::where('user_id',$id)->first();
         $user->completed=1;
         $user->save();
     return back()->with('success','Active this user');
    }
    public function userdeactive($id)
    {
        $user  =Activations::where('user_id',$id)->first();
         $user->completed=0;
         $user->save();
     return back()->with('success','Deactive this user');
    }
}
