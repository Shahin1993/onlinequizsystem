<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activations extends Model
{
    protected $fillable=['user_id','completed'];
}
