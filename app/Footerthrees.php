<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footerthrees extends Model
{
    protected $fillable=['title','link','priority'];
}
