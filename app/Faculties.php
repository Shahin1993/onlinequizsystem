<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculties extends Model
{
    protected $fillable=['faculty_name'];
    public function facultymembers()
    {
    	return $this->hasMany(Facultymembers::class);
    }
}
