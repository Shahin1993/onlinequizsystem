<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use View;
use App\Sliders;
use App\Faculties;
use App\Offices;
use App\Courses;
use App\Messages;
use App\Marqueenotices;
use App\Notices;
use App\Educations;
use App\Events;
use App\News;
use App\Categories;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
       view::composer('*',function($view){
          
           $sliders=Sliders::all();
           $view->with('sliders',$sliders);
           $faculties=Faculties::all();
           $view->with('faculties',$faculties);
           $offices=Offices::all();
           $view->with('offices',$offices);
           $courses=Courses::all();
           $view->with('courses',$courses);
           $messages=Messages::all();
           $view->with('messages',$messages);
           $marqueenotices=Marqueenotices::all();
           $view->with('marqueenotices',$marqueenotices);
           $notices=Notices::orderBy('id','desec')->get();
           $view->with('notices',$notices);
           $educations=Educations::orderBy('id','desec')->get();
           $view->with('educations',$educations);

        $events=Events::orderBy('id','desec')->get();
           $view->with('events',$events);
           
           $news=News::orderBy('id','desec')->get();
           $view->with('news',$news);
           $category=Categories::all();
           $view->with('category',$category);
       });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
