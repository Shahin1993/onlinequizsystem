<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coursefees extends Model
{
    protected $fillable=['title','image','details'];
}
