<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Events extends Model
{
	use SoftDeletes;
    protected $fillable=['title','description','featured_image','date','time','location','short_description'];
    public function eventimages()
    {
    	return $this->hasMany(Eventimages::class);
    }
}
