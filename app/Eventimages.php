<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Eventimages extends Model
{
	use SoftDeletes;
     protected $fillable=['events_id','image'];
}
