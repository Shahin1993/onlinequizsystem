<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footerones extends Model
{
    protected $fillable=['logo','institute_name','content','facebook_link','twitter_link','instagram_link','google_link','pinterest_link','linkedin_link','youtube_link'];
}
