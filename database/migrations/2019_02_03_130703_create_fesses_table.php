<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFessesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fesses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('department')->nullable();
            $table->string('entree_fee')->nullable();
            $table->string('semester_fee1')->nullable();
            $table->string('semester_fee2')->nullable();
            $table->string('total')->nullable();
            $table->longText('course_fee')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fesses');
    }
}
