<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaffsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('staffs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offices_id')->unsigned();
             $table->string('image')->default('default_faculty.jpg');
             $table->string('staff_name')->nullable();
             $table->string('degree')->nullable();
             $table->string('designation')->nullable();
             $table->string('department')->nullable();
             $table->string('institute')->nullable();
             $table->string('details')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('offices_id')->references('id')->on('offices');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('staffs');
    }
}
