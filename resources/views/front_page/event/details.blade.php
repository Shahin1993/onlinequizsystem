@extends('layouts.front_page.master')
@section('title','Events')
@section('style')
  
  <style>
     .mdalview ul {         
          padding:0 0 0 0;
          margin:0 0 0 0;
      }
      .mdalview ul li {     
          list-style:none;
          margin-bottom:0;  
          padding: 0; 
                 
      }
      .mdalview ul li img {
          cursor: pointer;
          height: 120px; 
          width: 100%;
      }
      .modal-body {
          padding:5px !important;
      }
      .modal-content {
          border-radius:0;
      }
      .modal-dialog img {
          text-align:center;
          margin:0 auto;
      }
    .controls{          
        width:50px;
        display:block;
        font-size:11px;
        padding-top:8px;
        font-weight:bold;          
    }
    .next {
        float:right;
        text-align:right;
    }
      /*override modal for demo only*/
      .modal-dialog {
          max-width:600px;
          padding-top: 90px;
      }
      @media screen and (min-width: 768px){
          .modal-dialog {
              width:500px;
              padding-top: 90px;
          }          
      }
      @media screen and (max-width:1500px){
          #ads {
              display:none;
          }
      }
      .modal-backdrop{
      	background-color:transparent;
      }
  </style>
@endsection
@section('banner')
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">NEWS & EVENTS</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>NEWS & EVENTS</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div><br>
@endsection
@section('content')
<div class="container">
<div class="row">
	<div class="col-md-12 mdalview" >
	<h1>{{$data->title}} </h1><br>
    <p style="text-align: justify;">{{$data->description}}</p>
    <ul class="row">
      @foreach($data->eventimages as $newimage)
            <li class="col-lg-2 col-md-2 col-sm-3 col-xs-4">
                <img class="img-responsive" src="{{asset('/images/event_images/'.$newimage->image)}}">
            </li>
@endforeach
          </ul>             
    </div> <!-- /container -->
    
     
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">         
          <div class="modal-body">                
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
 
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</div>
</div>
<br>
@endsection
@section('script')
<script src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="{{URL::asset('front_page/js/photo-gallery.js')}}"></script>
@endsection