@extends('layouts.front_page.master')
@section('title','Events')
@section('style')
<style type="text/css">
	 .section-padding{
        padding-top: 80px;
        padding-bottom: 10px;
    }

</style>
@endsection
@section('banner')
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">NEWS & EVENTS</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>NEWS & EVENTS</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div><br>
@endsection
@section('content')
<div class="latest-area section-padding bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title-wrapper">
<div class="section-title">
    <h3>News & Events</h3>
    <p></p>
</div>
</div>
</div>
</div>
<div class="row">
@foreach($events as $event)
<div class="col-md-6">
<div class="single-latest-item" >
<div class="single-latest-image">
    <a href="/event/details/{{$event->id}}"><img src="{{URL::asset('images/event_featuredimages/'.$event->featured_image)}}" alt=""></a>
</div>
<div class="single-latest-text" style="height: 227px;">
    <h3><a href="/event/details/{{$event->id}}">{{substr($event->title,0,25)}}</a></h3>
    <div class="single-item-comment-view">
       <span><i class="zmdi zmdi-calendar-check"></i>{{date('d F, Y', strtotime($event->date))}}</span> <span><i class="zmdi zmdi-pin"></i>{{$event->location}}</span>
   </div>
   <p>{{substr($event->short_description,0,100)}} .....</p>
   <a href="/event/details/{{$event->id}}" class="button-default">LEARN MORE</a>
</div>
</div>
</div>
@endforeach
</div>
</div>
</div>

@endsection