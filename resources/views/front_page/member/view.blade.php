@extends('layouts.front_page.master')
@section('title','Our Members')
@section('content')
<div class="container">
	<div class="teachers-area">
<div class="">
<div class="row">
<div class="col-md-12">
<div class="section-title-wrapper" style="margin-bottom:40px;">
<div class="section-title"><br>
    <h3>OUR MEMBERS</h3><p></p>
</div>
</div>
</div>
</div>

<div class="row">
@foreach($etsmember as $members)
<div class="col-lg-2 col-md-2 col-sm-4">
<div class="single-teacher-item">
<div class="single-teacher-image">
    <a href="#"><img src="{{asset('/images/member_images/'.$members->image)}}" alt=""></a>
</div>
<div class="single-teacher-text">
    <h5><a href="#">{{$members->first_name}}</a></h5>
    <span style="font-size:11px;">{{$members->education}}</span><br>
    <span style="font-size:12px;">
               Occupation:- {{$members->occupation}}<br>
      {{$members->organization_name}}
    </span>
    <div class="social-links">
        <a href="{{$members->facebook_link}}"><i class="zmdi zmdi-facebook"></i></a>
        <a href="{{$members->twitter_link}}"><i class="zmdi zmdi-twitter"></i></a>
        <a href="{{$members->google_link}}"><i class="zmdi zmdi-google-old"></i></a>
        <a href="{{$members->instagram_link}}"><i class="zmdi zmdi-instagram"></i></a>
    </div>
</div>
</div>
</div>
@endforeach
</div>
</div>
</div>
</div>
@endsection