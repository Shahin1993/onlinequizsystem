@extends('layouts.front_page.master')
@section('title','Join us')
@section('banner')
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">Join US</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>Join us</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div><br>
@endsection
@section('content')
 <div class="container">
  @include('front_page.messages.message')
 	<div class="row">
 		<div class="col-md-12">
 			<center><h2 >Member Registration Form</h2></center><hr>
 		</div>
 	</div>
 	<form action="/join-us" method="POST"  enctype="multipart/form-data">
  {{csrf_field()}}
 	<div class="row">
	<div class="col-md-2">
	</div>
	<div class="col-md-6">

	<div class="form-group">
      <label for="pwd">Your Name</label>
      <input type="text" name="first_name" class="form-control" placeholder="Please enter your Name" required="">
    </div>
    <div class="form-group">
      <label for="pwd">Contact Number</label>
       <input type="text" name="mobile" placeholder="Please enter your contract number" class="form-control">
    </div>
    <div class="form-group">
      <label for="pwd">Father's Name </label>
      <input type="text" name="father_name" class="form-control" placeholder="father name " required="">
    </div>
    <div class="form-group">
      <label for="pwd">Mother's Name</label>
      <input type="textt" name="mother_name" class="form-control" placeholder="mother name" >
    </div>
    <div class="form-group">
      <label for="pwd">Date of Birth </label>
      <input type="date" name="birth_date" class="form-control">
    </div>
    <div class="form-group">
      <label for="pwd">Address</label>
      <textarea class="form-control" name="address" placeholder="Address"></textarea>
    </div>
    <div class="form-group">
      <label for="pwd">Occupation : </label> &nbsp;&nbsp; &nbsp;&nbsp;
      <input type="radio" name="occupation" value="Student" required=""> Student&nbsp;&nbsp;
      <input type="radio" name="occupation" value="Job Holder" required=""> Job Holder
    </div>

    <div class="form-group">
      <label for="uname">Present School/College/University/Orgaization Name :  </label>
      <input type="text" name="organization_name" class="form-control" required="" placeholder="Please enter you organization name">
    </div>
     <div class="form-group">
      <label for="pwd">Educational Qualification : </label> &nbsp;&nbsp; &nbsp;&nbsp;
      <input type="text" name="education" class="form-control" placeholder="JSC/SSC/HSC/BSC/MSC"> 
    </div>
    

    <div class="form-group">
      <label for="pwd">Email (optional)</label>
       <input type="email" name="user_email" class="form-control" placeholder="please enter email ">
    </div>
    <div class="form-group">
      <label for="pwd">Join Date</label>
      <input type="Date" name="join_date" class="form-control" placeholder="Join Date" required="" value="{{date('Y-m-d')}}">
    </div>
    
    <div class="form-group">
      <label for="pwd">User ID (email/Mobile number):&nbsp;</label>
      <input type="text" name="email" placeholder="Email or Mobile number" required="" class="form-control">
    </div>
    
      <div class="form-group">
      <label for="pwd">Password:&nbsp;</label>
      <input type="password" name="password" placeholder="password at least 4 characters" required="" class="form-control">
    </div>

    <div class="form-group">
      <label for="pwd">Confirm Password:&nbsp;</label>
      <input type="password" name="confirm_password" placeholder="Please enter same password again" class="form-control" required="">
    </div>

    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember" required> I agree. Terms & Conditions
        
      </label>
    </div>
    
  
</div> <div class="col-md-3">
	<div class="form-group">
      <label for="pwd">Add Image :&nbsp;</label>
      <input type="file" name="image" onchange="loadfile(event)" required=""> 
      <img id="preview" width="100" height="100"  >
    </div>

<div class="form-group">
<label class="form-label"> Facebook Link</label>
<input type="text" name="facebook_link" class="form-control" placeholder="www.facebook.com/yourprofile">
</div>
<div class="form-group">
<label class="form-label"> Twitter Link</label>
<input type="text" name="twitter_link" class="form-control" placeholder="www.twitter.com/yourprofile">
</div>
<div class="form-group">
<label class="form-label"> Google Link</label>
<input type="text" name="google_link" class="form-control" placeholder="www.google.com/yourprofile">
</div>
<div class="form-group">
<label class="form-label"> Instagram Link</label>
<input type="text" name="instagram_link" class="form-control" placeholder="www.instagram.com/yourprofile">
</div>
</div>

 </div><!--row end-->
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-6">
		<button type="submit" class="btn btn-success form-control">Submit</button>
	</div>
</div>

</form>
 
</div>
<script type="text/javascript">
	function loadfile(event){
     var output=document.getElementById('preview');
     output.src=URL.createObjectURL(event.target.files[0]);
	};
</script>
@endsection