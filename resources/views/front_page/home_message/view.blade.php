@extends('layouts.front_page.master')
@section('title','Message')
@section('style')
<style type="text/css">
	   
</style>
@endsection
@section('banner')
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">{{$data->name}}</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>Message</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
@section('content')

<div class="container">
<div class="row">
<div class="col-md-12"><br>

<div class="row">
	<div class="col-md-4"></div>
	<div class="col-md-4"><center><img src="{{asset('/images/message_images/'.$data->image)}}">
		<h2 style="color:rgb(134, 188, 66);">{{$data->name}}</h2>
<h4 style="color:rgb(330, 192, 63,33);">{{$data->designation}}</h4></center>
	</div>
	<div class="col-md-4"></div>
</div><hr>
<span style="text-align: justify;margin-top: 10px;" class="drop-cap">{!!$data->description!!}</span>
</div>
</div>
</div><br>
@endsection