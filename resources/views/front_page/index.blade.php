@extends('layouts.front_page.master')
@section('title','Home')
@section('style')
<style type="text/css">
    .about-area{
        margin-top:5px;
    }
    .about-area h3{
        margin-bottom: 20px;
    }
    .section-padding{
        padding-top: 80px;
        padding-bottom: 10px;
    }

/*--testimonials--*/
#flexiselDemo1 {
    display: none;
}

.nbs-flexisel-container {
    position: relative;
    max-width: 100%;
}
.nbs-flexisel-ul {
    position: relative;
    width: 9999px;
    margin: 0px;
    padding: 0px;
    list-style-type: none;
}
.nbs-flexisel-inner {
    overflow: hidden;
    margin: 0px auto;
}
.nbs-flexisel-item {
    float: left;
    margin: 0;
    padding: 0px;
    position: relative;
    line-height: 0px;
}
.nbs-flexisel-item > img {
    cursor: pointer;
    position: relative;
}
/*-- Nav --*/
.nbs-flexisel-nav-left, .nbs-flexisel-nav-right {
    width: 36px;
    height: 36px;
    position: absolute;
    cursor: pointer;
    z-index: 100;
    border: 1px solid #0177b5;
}
.nbs-flexisel-nav-left {
    left: 46.5%;
    top: 109% !important;
    background:#000 url(images/themes.png) no-repeat 10px 10px;
}
.nbs-flexisel-nav-right {
    right: 46.5%;
    top: 109% !important;
    background:#000 url(images/themes1.png) no-repeat 11px 11px;
}
.laptop {
    background: #86bc42;
    padding: 2em 2em;
    /* border: 1px solid #f8b239; blue #0177b5; green #86bc42;*/
    margin: 0 15px;
}
.team-left{
    padding:0;
}
.team-right p,.colorful-tab-content p,.colorful-tab-content ul li {
    font-size: 15px;
    color: #fff;
    line-height: 2.2em;
    font-family: 'Raleway', sans-serif;
}
.team-right h5 {
    font-size: 17px;
    color: #fff;
    text-transform: uppercase;
    letter-spacing: 0.5px;
}
.name-w3ls {
    padding: 13px 30px 19px;
    display: inline-block;
    background: #2d3e50;
    margin-top: 1em;
}
.name-w3ls span {
    display: block;
    font-size: 13px;
    text-transform: capitalize;
    font-style: italic;
    letter-spacing: .8px;
    color: #ffffff;
    margin-top: 10px;
}
.flex-slider {
    margin: 0px auto 59px;
}
.team-left img {
    width: 100%;
    background:#2d3e50;
    border: 3px solid #2d3e50;
    padding: 10px;
}
</style>
@endsection
@section('content')
@include('front_page.messages.message')
<div class="slider-area">
<div class="preview-2">
<div id="nivoslider" class="slides" >
@foreach($sliders as $key=>$slider)
<img src="{{asset('/images/slider_images/'.$slider->image)}}" alt="" title=""/>
@endforeach
</div>  
</div>
</div>
</div>
<!--End of Slider Area-->

<!--course--->


<!--About Area Start--> 
<div class="about-area">
<div class="container">
<div class="row">
<div class="col-md-8">
<div class="about-container">
@foreach($educations->take(1) as $educat)
<h3>{{$educat->title}} ?</h3>
<p style="text-align: justify;"> {{substr($educat->short_description,0,500)}} </p>
<a class="button-default" href="/education/details/{{$educat->id}}">Learn More</a>
@endforeach
</div>
</div>
</div>
</div>
</div>
<!--End of About Area-->
   
<!--Latest News Area Start--> 
<div class="latest-area section-padding bg-white">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title-wrapper">
<div class="section-title">
    <h3>News & Events</h3>
    <p></p>
</div>
</div>
</div>
</div>
<div class="row">
@foreach($events->take(4) as $event)
<div class="col-md-6">
<div class="single-latest-item" >
<div class="single-latest-image">
    <a href="/event/details/{{$event->id}}"><img src="{{URL::asset('images/event_featuredimages/'.$event->featured_image)}}" alt=""></a>
</div>
<div class="single-latest-text" style="height: 227px;">
    <h3><a href="/event/details/{{$event->id}}">{{substr($event->title,0,25)}}</a></h3>
    <div class="single-item-comment-view">
       <span><i class="zmdi zmdi-calendar-check"></i>{{date('d F, Y', strtotime($event->date))}}</span> <span><i class="zmdi zmdi-pin"></i>{{$event->location}}</span>
   </div>
   <p>{{substr($event->short_description,0,100)}} .....</p>
   <a href="/event/details/{{$event->id}}" class="button-default">LEARN MORE</a>
</div>
</div>
</div>
@endforeach
</div>
</div>
</div>
<!--End of Latest News Area--> 
<!--Online Product Area Start-->

<!--testimonials-->
<div class="latest-area section-padding bg-white">
<div class="container">
<div id="testimonials" class="testimonials">
    <div class="container">
        <div class="row">
<div class="col-md-12">
<div class="section-title-wrapper">
<div class="section-title">
    <h3>Our Members Says</h3>
    <p>Sed lectus tellus , feugiat porttitor nulla.</p>
</div>
</div>
</div>
</div>
        <div class="flex-slider">
            <ul id="flexiselDemo1"> 
             @foreach($messages as $message)
                <li > 
                    <div class="laptop">
                        <div class="col-md-8 team-right">
                            <p style="text-align: justify;">{{substr($message->short_description,0,100)}}
                               </p>
                                 <a href="/m/{{$message->name}}/{{$message->id}}">
                            <div class="name-w3ls">
                              <h5>{{$message->name}}</h5>
                                <span>{{$message->designation}}</span>
                            </div></a>
                        </div>
                        <div class="col-md-4 team-left">
                            <img class="img-responsive" src="{{asset('/images/message_images/'.$message->image)}}" alt=" " />
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </li>
                @endforeach
            </ul>
            
        </div>

    </div>
</div>
</div>
</div>


<!--//testimonials-->
<!--End of Online Product Area-->
<!--Testimonial Area Start-->
<!-- <div class="testimonial-area">
<div class="container">
<div class="row">
<div class="col-lg-12 col-lg-offset-0 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1">
<div class="row">
<div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2">
    <div class="testimonial-image-slider text-center">
        <div class="sin-testiImage">
            <img src="{{URL::asset('front_page/img/testimonial/1.jpg')}}" alt="testimonial 1" />
        </div>
        <div class="sin-testiImage">
            <img src="{{URL::asset('front_page/img/testimonial/2.jpg')}}" alt="testimonial 2" />
        </div>
        <div class="sin-testiImage">
            <img src="{{URL::asset('front_page/img/testimonial/3.jpg')}}" alt="testimonial 3" />
        </div>
        <div class="sin-testiImage">
            <img src="{{URL::asset('front_page/img/testimonial/1.jpg')}}" alt="testimonial 1" />
        </div>
        <div class="sin-testiImage">
            <img src="{{URL::asset('front_page/img/testimonial/2.jpg')}}" alt="testimonial 2" />
        </div>
        <div class="sin-testiImage">
            <img src="{{URL::asset('front_page/img/testimonial/3.jpg')}}" alt="testimonial 3" />
        </div>
    </div>
</div>
</div> 
<div class="testimonial-text-slider text-center">
<div class="sin-testiText">
    <h2>M S Nawaz </h2>
    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
</div>
<div class="sin-testiText">
    <h2>Chowchilla Madera</h2>
    <p>Nam nec tellus a odio tincidunt This lorem is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean nisi sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum gravida.</p>
</div>
<div class="sin-testiText">
    <h2>Kattie Luis</h2>
    <p>Nam nec tellus a odio tincidunt This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean tincidunt sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum Photoshop.</p>
</div>
<div class="sin-testiText">
    <h2>M S Nawaz </h2>
    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.</p>
</div>
<div class="sin-testiText">
    <h2>Chowchilla Madera</h2>
    <p>Nam nec tellus a odio tincidunt This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, aliquet lorem quis tellus velit bibendum auctor, nisi elit consequat ipsum</p>
</div>
<div class="sin-testiText">
    <h2>Kattie Luis</h2>
    <p>Nam nec tellus a odio tincidunt This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem gravida tincidunt quis bibendum auctor, nisi elit consequat ipsum</p>
</div>
</div>   
</div>
</div>
</div>
</div> -->
<!--End of Testimonial Area-->
<!--Event Area Start-->

@endsection
@section('script')
<!--//testimonials-->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!--flexiselDemo1 -->
 <script type="text/javascript">
                            $(window).load(function() {
                                $("#flexiselDemo1").flexisel({
                                    visibleItems: 2,
                                    animationSpeed: 1000,
                                    autoPlay: true,
                                    autoPlaySpeed: 3000,            
                                    pauseOnHover: true,
                                    enableResponsiveBreakpoints: true,
                                    responsiveBreakpoints: { 
                                        portrait: { 
                                            changePoint:480,
                                            visibleItems: 1
                                        }, 
                                        landscape: { 
                                            changePoint:640,
                                            visibleItems: 1
                                        },
                                        tablet: { 
                                            changePoint:991,
                                            visibleItems: 1
                                        }
                                    }
                                });
                                
                            });
            </script>
            <script type="text/javascript" src="js/jquery.flexisel.js"></script>
@endsection