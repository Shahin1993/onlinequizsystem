@extends('layouts.front_page.master')
@section('title','gallery')
@section('style')
<style type="text/css">
     .section-padding{
        padding-top: 40px;
        padding-bottom: 10px;
    }

</style>
@endsection
@section('banner')
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">Gallery</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>Gallery</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
@section('content')
    <div class="row">
    <div class="product-area section-padding">
<div class="container">
    <div class="row">
    <div class="col-md-12"><h1 style="text-align: center;">OUR GALLERY</h1></div>
</div><hr>
<div class="row">
@foreach($events as $event)
<div class="col-md-3 hidden-sm" style="height:350px;">
<div class="single-product-item">
<div class="single-product-image">
<a href="/event/details/{{$event->id}}"><img src="{{URL::asset('/images/event_featuredimages/'.$event->featured_image)}}" alt="image"></a>
</div>
<div class="single-product-text">
<h4><a href="/event/details/{{$event->id}}">{{$event->title}}</a></h4>
</div>
</div>
</div>
@endforeach
</div>

</div>
</div>
</div>
    @endsection