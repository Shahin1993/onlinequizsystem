@extends('layouts.front_page.master')
@section('title','Our Service')
@section('style')

	<style type="text/css">
	.agileinfo_services_grid{
		margin-bottom: 15px;
		margin-top: 15px;
	}
	.agileinfo_services_grid1{
	padding:2em;
	border:1px solid #999;
	text-align:center;
	-webkit-transition: all 300ms ease-in-out;
    -moz-transition: all 300ms ease-in-out;
    -ms-transition: all 300ms ease-in-out;
    -o-transition: all 300ms ease-in-out;
    transition: all 300ms ease-in-out;
    z-index: 1;
    width: 100%;
    overflow: hidden;
    position: relative;
}
.agileinfo_services_grid1:after, .agileinfo_services_grid1:before {
    content: '';
    z-index: 2;
    width: 0;
    height: 0;
    visibility: hidden;
    position: absolute;
    -webkit-transition: all 800ms ease-in-out;
    -moz-transition: all 800ms ease-in-out;
    -ms-transition: all 800ms ease-in-out;
    -o-transition: all 800ms ease-in-out;
    transition: all 800ms ease-in-out;
}
.agileinfo_services_grid1:before {
    bottom: 0;
    right: 0;
    border-bottom: 1px solid #0177b5;
    border-right: 1px solid #0177b5;
}
.agileinfo_services_grid1:after {
    top: 0;
    left: 0;
    border-top: 1px solid #0177b5;
    border-left: 1px solid #0177b5;
}
.agileinfo_services_grid1:hover {
    cursor: default;
	border: 1px solid #fff;
}
.agileinfo_services_grid1:hover:after, .agileinfo_services_grid1:hover:before {
    width: 100%;
    height: 100%;
    visibility:visible;
}
.agileinfo_services_grid1 i {
    font-size: 2.5em;
    color: #f8b239;
    top: 0;
}
.agileinfo_services_grid1:hover i {
	transition: all .5s ease;
	-webkit-transition: all .5s ease;
	-moz-transition: all .5s ease;
	-o-transition: all .5s ease;
	-ms-transition: all .5s ease;
    text-shadow: 2px 2px #4f565a;
	-webkit-text-shadow: 2px 2px #4f565a;
	-moz-text-shadow: 2px 2px #4f565a;
	-o-text-shadow: 2px 2px #4f565a;
	-ms-text-shadow: 2px 2px #4f565a;
}
.agileinfo_services_grid1 h4{
	text-transform: capitalize;
    font-size: 1.3em;
    line-height: 1.5em;
    margin: 1em 0;
    color:#0177b5;
}
.agileinfo_services_grid1 p{
	color:#999;
	line-height:2em;
}
.agileinfo_services_grid1:hover p{
	color:#000;
	line-height:2em;
}
.agileinfo_services_grid:nth-child(7),.agileinfo_services_grid:nth-child(5),.agileinfo_services_grid:nth-child(6){
	margin:2em 0 0;
}
	</style>
@endsection
@section('banner')
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">Our Services</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>Service</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
@section('content')


<div class="agileinfo_services_grids inner-padding">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title-wrapper" style="margin-bottom:40px;">
<div class="section-title"><br>
    <h3>OUR SERVICES</h3><p>Human Service is The Ultimate Religion</p>
</div>
</div>
</div>
</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<i class="fa fa-female" aria-hidden="true"></i>
						<h4>Mother’s Helpers</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<i class="fa fa-group" aria-hidden="true"></i>
						<h4>Friendly Support</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<i class="fa fa-book" aria-hidden="true"></i>
						<h4>Educational Events</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<i class="fa fa-paint-brush" aria-hidden="true"></i>
						<h4>Art Programs</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<i class="fa fa-group" aria-hidden="true"></i>
						<h4>Social Programs</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>
				<div class="col-md-4 agileinfo_services_grid">
					<div class="agileinfo_services_grid1">
						<i class="fa fa-trophy" aria-hidden="true"></i>
						<h4>Special Olympics</h4>
						<p>Quisque consectetur, sem id sagittis sodales, augue diam consequat mi, 
							sed suscipit turpis diam eget nisl.</p>
					</div>
				</div>

@foreach($service as $data)

<div class="col-md-4 agileinfo_services_grid ">
					<div class="agileinfo_services_grid1">
						<i class="{{$data->icon}}" aria-hidden="true"></i>
						<a href="/service/{{$data->title}}"><h4>{{$data->title}}</h4></a>
						<p>{{$data->short_description}} </p>
						<p><a href="#">Link</a></p>
					</div>
				</div>
@endforeach
				<div class="clearfix"> </div>
	</div>
</div>


@endsection