@extends('layouts.front_page.master')
@section('title','Contact')
@section('banner')
<br>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
 
@endif
<div class="breadcrumb-banner-area">
    <div class="container">
    <div class="row">
    <div class="col-md-12">
    <div class="breadcrumb-text">
    <h1 class="text-center">CONTACT US</h1>
    <div class="breadcrumb-bar">
    <ul class="breadcrumb text-center">
    <li><a href="/">Home</a></li>
    <li>CONTACT</li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
@endsection
@section('content')
<div class="google-map-area">
    <!--  Map Section -->
    <div id="contacts" class="map-area">
    <div id="" style="width:100%;height:485px;filter: grayscale(100%);-webkit-filter: grayscale(100%);"><iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14720.42211489707!2d90.3405064!3d22.7243187!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc82512a0c233dd58!2sinfra+polytechnic+institute!5e0!3m2!1sen!2sbd!4v1548769114311" width="100%" height="450px" frameborder="0" style="border:0" allowfullscreen></iframe></div>
    </div>
    </div>
    
    <div class="contact-form-area section-padding">
    <div class="container">
    <div class="row">
    <div class="col-md-4">
    <h4 class="contact-title">contact info</h4>
     @foreach($contactAddress->take(1) as $data)
    <div class="contact-text">
    <p><span class="c-icon"><i class="zmdi zmdi-phone"></i></span><span class="c-text">{{$data->mobile}}</span></p>
    <p><span class="c-icon"><i class="zmdi zmdi-email"></i></span><span class="c-text">{{$data->email}}</span></p>
    <p><span class="c-icon"><i class="zmdi zmdi-pin"></i></span><span class="c-text">{{\Illuminate\Support\Str::words($data->address,4,'.')}}<br>
    {{substr($data->address,-18)}}</span></p>
    </div>
    <h4 class="contact-title">social media</h4>
    <div class="link-social">
    <a href="{{$data->facebook}}"><i class="zmdi zmdi-facebook"></i></a>
    <a href="{{$data->whatsapp}}"><i class="zmdi zmdi-rss"></i></a>
    <a href="{{$data->twiter}}"><i class="zmdi zmdi-google-plus"></i></a>
    <a href="{{$data->google}}"><i class="zmdi zmdi-pinterest"></i></a>
    <a href="{{$data->instragram}}"><i class="zmdi zmdi-instagram"></i></a>
    </div>
     @endforeach
    </div>
    <div class="col-md-7">
    <h4 class="contact-title">send your massage</h4>
    <form id="contact-form" action="/contacts" method="POST">
 {{csrf_field()}}
    <div class="row">
    <div class="col-md-6">
    <input type="text" name="name" placeholder="name">
    </div>
    <div class="col-md-6">
    <input type="text" name="phone" placeholder="Phone Number">
    </div>
    <div class="col-md-12">
    <input type="email" name="email" placeholder="Email">
    </div>
    <div class="col-md-12">
    <input type="text" name="subject" placeholder="Subject">
    </div>
    <div class="col-md-12">
    <textarea name="details" cols="30" rows="10" placeholder="Message"></textarea>
    <button type="submit" class="button-default">SEND</button>
    </div>
    </div>
    </form>
    <p class="form-messege"></p>
    </div>
    </div>
    </div>
    </div>
    @endsection