@extends('layouts.front_page.master')
@section('title','Faculty')
@section('style')
<style type="text/css">
.single-teacher-text{
    min-height:150px!important;
}
</style>
@endsection
@section('content')
<div class="container">
	<div class="teachers-area">
<div class="container">
<div class="row">
<div class="col-md-12">
<div class="section-title-wrapper" style="margin-bottom:40px;">
<div class="section-title"><br>
    <h3>OUR OFFICERS & STAFFS</h3>
    <p></p>
</div>
</div>
</div>
</div>
@foreach($offices as $data)
<div class="row">
<h2><center> {{$data->office_name}}</center></h2><hr>
@foreach($data->staffs as $members)
@if($members->offices_id=$data->id)
<div class="col-lg-2 col-md-2 col-sm-3">
<div class="single-teacher-item">
<div class="single-teacher-image">
    <a href="#"><img src="{{asset('images/staff_images/'.$members->image)}}" alt=""></a>
</div>
<div class="single-teacher-text">
    <h5><a href="#">{{$members->staff_name}}</a></h5>
    <span style="font-size:11px;">{{$members->designation}}</span><br>
    <span style="font-size:12px;">
     
       Infra Polytechnic Istitute, Barishal
    </span>
    <!--<div class="social-links">-->
    <!--    <a href="#"><i class="zmdi zmdi-facebook"></i></a>-->
    <!--    <a href="#"><i class="zmdi zmdi-twitter"></i></a>-->
    <!--    <a href="#"><i class="zmdi zmdi-google-old"></i></a>-->
    <!--    <a href="#"><i class="zmdi zmdi-instagram"></i></a>-->
    <!--</div>-->
</div>
</div>
</div>
@endif
@endforeach
</div><br><br>
@endforeach
</div>
</div>
</div>
@endsection