@extends('layouts.front_page.master')
@section('title','Faculty')
@section('content')
<div class="container">
<div class="row">
<div class="col-md-12"><br>
<h2 style="color:rgb(134, 188, 66);"><center>{{$data->course_title}}</center></h2><hr>
<p><img src="{{URL::asset('images/course_images/'.$data->course_image)}}" alt=""></p>
<span style="text-align: justify;">{!!$data->course_description!!}</span>
</div>
</div>
</div>
@endsection