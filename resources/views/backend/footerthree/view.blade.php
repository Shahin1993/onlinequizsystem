 @extends('layouts.admin.master')
 @section('title','footer three view')
 @section('content')
 <div class="page-header">
<h1>
Footer  View 
<small>
<i class="ace-icon fa fa-angle-double-right"></i>
 view
</small>
&nbsp;<a href="/footerthrees/create">Add </a>
</h1>
</div><!-- /.page-header -->
 @include('admin.messages.message')
 
<div class="row">
<table class="table table-bordered">
	<thead>
	<tr>
		<th>SL</th>
		<th> Title</th>
		<th>Link</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr></thead><tbody>
@foreach($footerthrees as $key=>$data)
<tr>
	<td>{{++$key}}</td>
	 
	<td>{{$data->title}}  </td>
	<td>{{$data->link}}</td>
	 
	<td>
		
		<a href="/footerthrees/{{$data->id}}/edit"><button class="btn btn-info btn-lg">Edit</button></a></td><td>{{Form::open(['url'=>'/footerthrees/'.$data->id,'method'=>'Delete'])}}
           <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure Delete This Data?')">Delete</button>
              {{Form::close()}}</td>
	 
</tr>

@endforeach
</tbody>
</table>
</div>
 @endsection