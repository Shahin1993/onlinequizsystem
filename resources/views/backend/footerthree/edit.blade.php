@extends('layouts.admin.master')
@section('title','footer three edit')
@section('content')

<div class="page-header">
<h1>
Footer three  update
<small>
Footer three  for front page </small>&nbsp;<a href="/footerthrees">  View</a>
</h1>
</div><!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
 @include('admin.messages.message')
{{Form::open(['url'=>'/footerthrees/'.$data->id ,'method'=>'PATCH','class'=>'form-horizontal','enctype'=>'multipart/form-data'])}}

<div class="space-4"></div>

<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Title</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="title" class="col-xs-10 col-sm-5" name="title" required="" value="{{$data->title}}" />
		
	</div>
</div>
 <div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Link</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="link" class="col-xs-10 col-sm-5" name="link" required="" value="{{$data->link}}" />
		
	</div>
</div>
 
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Priority</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="priority" class="col-xs-10 col-sm-5" name="priority" required="" value="{{$data->Priority}}" />
		
	</div>
</div>
 

<div class="space-4"></div>
<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			Submit
		</button>

		&nbsp; &nbsp; &nbsp;
		<button class="btn" type="reset">
			<i class="ace-icon fa fa-undo bigger-110"></i>
			Reset
		</button>
	</div>
</div>

<div class="hr hr-24"></div>

 
{{Form::close()}}

 <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div>
@endsection