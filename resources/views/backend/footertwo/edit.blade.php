@extends('layouts.admin.master')
@section('title','footer twp add')
@section('content')

<div class="page-header">
<h1>
Footer two  Add
<small>
Footer two  for front page </small>&nbsp;<a href="/footertwos">View</a>
</h1>
</div><!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
 @include('admin.messages.message')
{{Form::open(['url'=>'/footertwos/'.$data->id ,'method'=>'PATCH','class'=>'form-horizontal','enctype'=>'multipart/form-data'])}}

<div class="space-4"></div>

<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Title</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="title" class="col-xs-10 col-sm-5" name="title" required="" value="{{$data->title}}" />
		
	</div>
</div>
 <div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Phone</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="phone" class="col-xs-10 col-sm-5" name="phone" required="" value="{{$data->phone}}" />
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Mobile</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="mobile number" class="col-xs-10 col-sm-5" name="mobile" required="" value="{{$data->mobile}}" />
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Email</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="email" class="col-xs-10 col-sm-5" name="email" required="" value="{{$data->email}}" />
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Location </label>
	<div class="col-sm-9">
		<textarea name="location" class="col-xs-10 col-sm-5" required="">{{$data->location}}</textarea>
		
	</div>
</div>

<div class="space-4"></div>
<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			Update
		</button>

		&nbsp; &nbsp; &nbsp;
		<button class="btn" type="reset">
			<i class="ace-icon fa fa-undo bigger-110"></i>
			Reset
		</button>
	</div>
</div>

<div class="hr hr-24"></div>

 
{{Form::close()}}

 <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div>
@endsection