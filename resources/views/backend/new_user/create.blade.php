@extends('layouts.admin.master')
@section('title','New User Create')
@section('content')

<div class="page-header">
<h1>
User Create
<small>
<i class="ace-icon fa fa-angle-double-right"></i>
New user create  for Admin page </small>&nbsp;<!-- <a href="/days">Day View</a> -->
</h1>
</div>
@include('admin.messages.message')
	<div class="row">
<div class="col-xs-12">
  <form class="form-horizontal" action="/newuser" method="POST" role="form">
    {{csrf_field()}}
  	<div class="form-group">
      <label class="control-label col-sm-2 no-padding-right" for="first_name">Name:</label>
      <div class="col-sm-10">
        <input type="text"  id="first_name" placeholder="Enter your  name" name="first_name" class="col-xs-10 col-sm-5" required="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 no-padding-right" for="mobile">mobile No:</label>
      <div class="col-sm-10">
        <input type="text"  id="mobile" placeholder="Enter your mobile number" name="mobile" class="col-xs-10 col-sm-5">
      </div>
    </div>
     <!-- <div class="form-group">
      <label class="control-label col-sm-2" for="email">User Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="user_email">
      </div>
    </div> -->
    <div class="form-group">
  <label class="col-sm-2 control-label no-padding-right" for="form-field-1">User Role</label>

  <div class="col-sm-10">
         <select name="slug" class="col-xs-10 col-sm-5" required="">
      <option value="" >select one</option>
            @foreach(Sentinel::getRoleRepository()->get() as $role)
      <option value="{{$role->slug}}">{{$role->slug}}</option>
      @endforeach
    </select>
  </div>
</div>
    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="email">Email/user id/user name:</label>
      <div class="col-sm-10">
        <input type="text" id="email" placeholder="Enter email" name="email" class="col-xs-10 col-sm-5" required="">
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label no-padding-right" for="password">Password:</label>
      <div class="col-sm-10">          
        <input type="password"  id="password" placeholder="Enter password" name="password" class="col-xs-10 col-sm-5" required="">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 no-padding-right" for="confirm_password">Confirm Password:</label>
      <div class="col-sm-10">          
        <input type="password" id="confirm_password" placeholder="Enter password again" name="confirm_password" class="col-xs-10 col-sm-5" required="">
      </div>
    </div>
    
    <div class="space-4"></div>

<div class="clearfix form-actions">
  <div class="col-md-offset-3 col-md-9">
    <button class="btn btn-info" type="submit">
      <i class="ace-icon fa fa-check bigger-110"></i>
      Submit
    </button>

    &nbsp; &nbsp; &nbsp;
    <button class="btn" type="reset">
      <i class="ace-icon fa fa-undo bigger-110"></i>
      Reset
    </button>
  </div>
</div>

<div class="hr hr-24"></div>
  </form>
	
	</div>
</div>

@endsection