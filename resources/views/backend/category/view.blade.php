@extends('layouts.admin.master')
@section('title','location view')
@section('content')

            <div class="row">
<div class="col-md-12">
<!-- BEGIN EXAMPLE TABLE PORTLET-->
<div class="portlet light portlet-fit bordered">
<div class="portlet-title">
<div class="caption">
<i class="icon-bubble font-red"></i>
<span class="caption-subject font-red sbold uppercase">Categories View</span> <br>
<span class="caption-subject font-red sbold uppercase">total add = {{ $category->count() }}</span>
 
</div>
 

</div>
<div class="portlet-body">
<div class="table-toolbar">
<div class="row">
<div class="col-md-6">
    <div class="btn-group"><a href="{{url('/categories/create')}} ">
        <button class="btn green">  Add New
            <i class="fa fa-plus"></i>
        </button></a>
    </div>
</div>



</div>
</div>
<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
<tr>
    <th>SL</th>
    <th> Location </th>
    <th> Edit </th>
    <th> Delete </th>
</tr>
</thead>
<tbody>
     @foreach($category as $key=>$data)
<tr>
    <td>{{++$key}} </td>
    <td> {{$data->category}} </td>
    
    <td>
        <a class="edit" href="/categories/{{$data->id}}/edit "> Edit </a>
    </td>
    <td>
        {{Form::open(['url'=>'/categories/'.$data->id,'method'=>'Delete'])}}
                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure delete this?')">Delete</button>
              {{Form::close()}}
    </td>
</tr>
  @endforeach
</tbody>
</table>
  
</div>

</div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                            </div>

                        </div>
@endsection