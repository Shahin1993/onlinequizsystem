@extends('layouts.admin.master')
@section('title','Category edit')
@section('content')
@if (session('warning'))
    <div class="alert alert-danger">
        {{ session('warning') }}
    </div>
    @elseif (Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
 
@endif

<div class="row">
        <div class="col-md-12">

            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Categories Update</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <!-- <form action="{{url('/locations')}} " id="form_sample_2" class="form-horizontal" method="POST" >
                      {{ csrf_field() }} -->
                      {{Form::open(['url'=>'/categories/'.$data->id ,'method'=>'PATCH','class'=>'form-horizontal'])}}
                        <div class="form-body">
                             
                            <div class="form-group  margin-top-20">
                                <label class="control-label col-md-3">Category
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control" name="category" value="{{$data->category}}" /> 
                                        @if($errors->has('location'))
<span class="text-danger">{{$errors->first('location')}} </span>
@endif
 


                                    </div>
                                </div>
                            </div>
                              
 
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Update</button>
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
                   <!--  </form> -->
                   {{Form::close()}}
                            <!-- END FORM-->
                        </div>
                    </div>
                     
                </div>
            </div>

@endsection