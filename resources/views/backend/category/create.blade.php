@extends('layouts.admin.master')
@section('title','Category Add')
@section('content')

@if (session('warning'))
    <div class="alert alert-danger">
        {{ session('warning') }}
    </div>
    @elseif (Session::has('success'))
    <div class="alert alert-success">
        {{ Session::get('success') }}
    </div>
 
@endif

<div class="row">
        <div class="col-md-12">

            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-green"></i>
                        <span class="caption-subject font-green bold uppercase">Category Add</span>
                    </div>
                    
                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <!-- <form action="{{url('/locations')}} " id="form_sample_2" class="form-horizontal" method="POST" >
                      {{ csrf_field() }} -->
                      {{Form::open(['url'=>'/categories','method'=>'POST','class'=>'form-horizontal'])}}
                        <div class="form-body">
                             
                            <div class="form-group  margin-top-20">
                                <label class="control-label col-md-3">Category
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <div class="input-icon right">
                                        <i class="fa"></i>
                                        <input type="text" class="form-control" name="category" placeholder="Dhaka" /> 
                                        @if($errors->has('category'))
<span class="text-danger">{{$errors->first('category')}} </span>
@endif
  

                                    </div>
                                </div>
                            </div>
                              
 
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green">Save</button>
                                    <button type="reset" class="btn default">Reset</button>
                                </div>
                            </div>
                        </div>
                   <!--  </form> -->
                   {{Form::close()}}
                            <!-- END FORM-->
                        </div>
                    </div>
                     
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
<thead>
<tr>
    <th>SL</th>
    <th> Category </th>
    <th> Edit </th>
    <th> Delete </th>
</tr>
</thead>
<tbody>
     @foreach($category as $key=>$data)
<tr>
    <td>{{++$key}} </td>
    <td> {{$data->category}} </td>
    
    <td>
        <a class="edit" href="/categories/{{$data->id}}/edit "> Edit </a>
    </td>
    <td>
        {{Form::open(['url'=>'/categories/'.$data->id,'method'=>'Delete'])}}
                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure delete this?')">Delete</button>
              {{Form::close()}}
    </td>
</tr>
  @endforeach
</tbody>
</table>
                </div>
            </div>

@endsection