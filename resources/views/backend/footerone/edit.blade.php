@extends('layouts.admin.master')
@section('title','footer one edit')
@section('content')

<div class="page-header">
<h1>
Footer one  update
<small>
Footer one  for front page </small>&nbsp;<a href="/footerones">Footer one   View</a>
</h1>
</div><!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
 @include('admin.messages.message')

 {{Form::open(['url'=>'/footerones/'.$data->id ,'method'=>'PATCH','class'=>'form-horizontal','enctype'=>'multipart/form-data'])}}

<div class="space-4"></div>

<!-- <div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Logo</label>
	<div class="col-sm-9">
		<input type="file" id="form-field-2" placeholder="Mobile Number" class="col-xs-10 col-sm-5" name="logo" required="" />
		
	</div>
</div> -->
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Intitute name</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="institute name" class="col-xs-10 col-sm-5" name="institute_name" required="" value="{{$data->institute_name}}" />
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Content </label>
	<div class="col-sm-9">
		<textarea name="content" class="col-xs-10 col-sm-5" required="">{{$data->content}}</textarea>
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Facebook </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://facebook.com/...." class="col-xs-10 col-sm-5" name="facebook_link"  value="{{$data->facebook_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Twitter </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://Twitter.com/...." class="col-xs-10 col-sm-5" name="twitter_link" value="{{$data->twitter_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Instagram </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://instagram.com/...." class="col-xs-10 col-sm-5" name="instagram_link" value="{{$data->instagram_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Google Plus</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://plush.google.com/...." class="col-xs-10 col-sm-5" name="google_link" value="{{$data->google_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Pinterest </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://pinterest.com/...." class="col-xs-10 col-sm-5" name="pinterest_link" value="{{$data->pinterest_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Whatsapp </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://whatsapp.com/...." class="col-xs-10 col-sm-5" name="whatsapp_link" value="{{$data->whatsapp_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Linkedin </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://kinkedin.com/...." class="col-xs-10 col-sm-5" name="linkedin_link" value="{{$data->linkedin_link}}" />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> YouTube </label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="http://youtube.com/...." class="col-xs-10 col-sm-5" name="youtube_link" value="{{$data->youtube_link}}" />
	</div>
</div>
<div class="space-4"></div>
<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			Update
		</button>

		&nbsp; &nbsp; &nbsp;
		<button class="btn" type="reset">
			<i class="ace-icon fa fa-undo bigger-110"></i>
			Reset
		</button>
	</div>
</div>

<div class="hr hr-24"></div>

 
{{Form::close()}}

 <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div>
@endsection