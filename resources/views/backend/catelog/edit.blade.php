@extends('layouts.admin.master')
@section('title','Catelog Update')
@section('content')

<div class="page-header">
<h1>
Catelog  Update
<small>
<i class="ace-icon fa fa-angle-double-right"></i>
Catelog for front page </small>&nbsp;<a href="/catelogs">Catelog  View</a>
</h1>
</div><!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
 @include('admin.messages.message')
{{Form::open(['url'=>'/catelogs/'.$data->id ,'method'=>'PATCH','class'=>'form-horizontal','enctype'=>'multipart/form-data'])}}
<div class="space-4"></div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2"> Category </label>
	<div class="col-sm-9">
		 
		<select class="col-xs-10 col-sm-5" name="categories_id" required="">
			<option value="{{$data->categories_id}}">{{$data->categories->category}}</option>
			<
			@foreach($category as $cat)
			<option value="{{$cat->id}}">{{$cat->category}}</option>
			@endforeach
		</select>
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Catelog</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder="catelog" class="col-xs-10 col-sm-5" name="catelog" required="" value="{{$data->catelog}}" />
		
	</div>
</div>

<div class="space-4"></div>
<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			Update
		</button>

		&nbsp; &nbsp; &nbsp;
		<button class="btn" type="reset">
			<i class="ace-icon fa fa-undo bigger-110"></i>
			Reset
		</button>
	</div>
</div>

<div class="hr hr-24"></div>

 
{{Form::close()}}
 <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div>
@endsection