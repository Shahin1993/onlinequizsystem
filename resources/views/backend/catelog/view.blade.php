 @extends('layouts.admin.master')
 @section('title','Catelog view')
 @section('content')
 <div class="page-header">
<h1>
Catelog View 
<small>
<i class="ace-icon fa fa-angle-double-right"></i>
All Catelog view
</small>
&nbsp;<a href="/catelogs/create">Add Catelog</a>
</h1>
</div><!-- /.page-header -->
 @include('admin.messages.message')
 
<div class="row">
<table class="table table-bordered">
	<thead>
	<tr>
		<th>SL</th>
		<th>Category Name</th>
		<th>Catelog Name</th>
		<th>Edit</th>
		<th>Delete</th>
	</tr></thead><tbody>
@foreach($catelog as $key=>$data)
<tr>
	<td>{{++$key}}</td>
	 
	<td>{{$data->categories->category}}  </td>
	<td>{{$data->catelog}}  </td>
	 
	<td>
		
		<a href="/catelogs/{{$data->id}}/edit"><button class="btn btn-info btn-lg">Edit</button></a> </td>
              <td>{{Form::open(['url'=>'/catelogs/'.$data->id,'method'=>'Delete'])}}
           <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure Delete This Data?')">Delete</button>
              {{Form::close()}}</td>
	 
</tr>

@endforeach
</tbody>
</table>
</div>
 @endsection