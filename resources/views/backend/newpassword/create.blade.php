@extends('layouts.admin.master')
@section('title','password reset')
@section('content')

<div class="row">
	@include('admin.messages.message')
	<div class="col-md-6">
		<form class="form-horizontal" action="password-reset" method="post">
			{{csrf_field()}}
			<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Student/Teacher ID</label>
	<div class="col-sm-9">
		<input type="text" id="form-field-2" placeholder=" user id" class="col-xs-10 col-sm-5" name="student_id"  />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">New password</label>
	<div class="col-sm-9">
		<input type="password" id="form-field-2" placeholder="******" class="col-xs-10 col-sm-5" name="password"  />
	</div>
</div>
<div class="form-group">
	<label class="col-sm-3 control-label no-padding-right" for="form-field-2">Confirm Password</label>
	<div class="col-sm-9">
		<input type="password" id="form-field-2" placeholder=" ****" class="col-xs-10 col-sm-5" name="confirm_password"  />
	</div>
</div>
<div class="space-4"></div>
<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			Submit
		</button>

		&nbsp; &nbsp; &nbsp;
		<button class="btn" type="reset">
			<i class="ace-icon fa fa-undo bigger-110"></i>
			Reset
		</button>
	</div>
</div>
		</form>
	</div>
</div>
@endsection