 @extends('layouts.admin.master')
 @section('title','Books view')
 @section('content')
 <div class="page-header">
<h1>
 Books View 
<small>
<i class="ace-icon fa fa-angle-double-right"></i>
All Book view
</small>
&nbsp;<a href="/books/create">Add </a>
</h1>
</div><!-- /.page-header -->
 @include('admin.messages.message')
 
<div class="row">
	<div class="table-responsive">
<table class="table table-bordered" id="dataTables">
	<thead>
	<tr>
		<th>SL</th>
		<th>Title</th>
		<th>Description</th>
		<th>Catelog</th>
		<th>view</th>
		<th>Details</th>
		<th>edit</th>
		<th>delete</th>
	</tr></thead><tbody>
	    	<?php $i=$books->perPage()*($books->currentPage()-1);?>
@foreach($books as $key=>$data)
<tr>
	<td><?php $i++; ?> {{$i}}</td>
	<td>{{$data->title}}</td>
	<td>{{ \Illuminate\Support\Str::words($data->short_description, 20,'....')  }} </td>
	<td>{{$data->catelog}}</td>
	<td>{{$data->view}}</td>
	 <td>
		
		<a href="/books/{{$data->id}}"><button class="btn btn-info btn-lg">view</button></a></td>
	<td>
		
		<a href="/books/{{$data->id}}/edit"><button class="btn btn-info btn-lg">Edit</button></a></td>
		<td>
			{{Form::open(['url'=>'/books/'.$data->id,'method'=>'Delete'])}}
           <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure Delete This Data?')">Delete</button>
              {{Form::close()}}
		</td>
	 
</tr>

@endforeach
</tbody>
</table>
{{$books->links()}}
</div>
</div>
 @endsection