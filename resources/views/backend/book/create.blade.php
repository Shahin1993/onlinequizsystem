@extends('layouts.admin.master')
@section('title','Book add')
@section('content')

<div class="page-header">
<h1>
Book  Add
<small>
Book  for front page </small>&nbsp;<a href="/books"> View</a>
</h1>
</div><!-- /.page-header -->
<div class="row">
<div class="col-xs-12">
<!-- PAGE CONTENT BEGINS -->
 @include('admin.messages.message')

<form class="form-horizontal" role="form" action="/books" method="POST" enctype="multipart/form-data">
	{{csrf_field()}}

<div class="space-4"></div>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Category</label>
	<div class="col-sm-10">
		<select class="form-control" name="categories_id" required="" id="category">
			<option value="">Please Select a Category</option>
			@foreach($category as $cat)
			<option value="{{$cat->id}}">{{$cat->category}} </option>
			@endforeach
		</select>
	 
		
	</div>
</div>
<div class="space-4"></div>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Catelog</label>
	<div class="col-sm-10">
		<select class="form-control" name="catelog" required="" id="catelog">
			 
			<option value="">Please Select a Catelog</option>
			 
		</select>
		 
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Title</label>
	<div class="col-sm-10">
		<input type="text" id="form-field-2" placeholder="title" class="form-control" name="title" required="" />
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Short Description </label>
	<div class="col-sm-10">
		<textarea name="short_description" class="form-control" required="" placeholder="short Description"></textarea>
		
	</div>
</div>
 
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Description </label>
	<div class="col-sm-10">
		<textarea name="description" class="form-control" required="" id="summernote"></textarea>
		
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Tag </label>
	<div class="col-sm-10">
		<textarea name="tag" class="form-control" required="" placeholder="tag"></textarea>
		
	</div>
</div>

<div class="form-group">
	<label class="col-sm-2 control-label no-padding-right" for="form-field-2">Read Images :</label>
	<div class="col-sm-10">
		<input type="file" id="form-field-2" class="col-xs-10 col-sm-5" class="form-control" name="image[]" multiple="" />
	</div>
</div>

<div class="space-4"></div>
<div class="clearfix form-actions">
	<div class="col-md-offset-3 col-md-9">
		<button class="btn btn-info" type="submit">
			<i class="ace-icon fa fa-check bigger-110"></i>
			Submit
		</button>

		&nbsp; &nbsp; &nbsp;
		<button class="btn" type="reset">
			<i class="ace-icon fa fa-undo bigger-110"></i>
			Reset
		</button>
	</div>
</div>

<div class="hr hr-24"></div>

 
</form>

 <!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div>



@endsection
@section('script')


            <script type="text/javascript">
                
                $('#category').on('change',function(e){
                    console.log(e);

                    var categories_id= e.target.value;

                    $.get('/json-catelogs?categories_id=' + categories_id,function(data){
                        console.log(data);

                    $('#catelog').empty();
          $('#catelog').append('<option value="" disable="true" selected="true">Select a Catelog</option>');

          $.each(data, function(index, districtsObj){
            $('#catelog').append('<option value="'+ districtsObj.catelog +'">'+ districtsObj.catelog +'</option>');
          });
         
                    });
                });
            </script>

<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#summernote').summernote({
			height:'200px',
			placeholder:'Description',
			toolbar: [
    ['style', ['fontname','bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr', 'readmore']],
            ['genixcms', ['elfinder']],
            ['view', ['fullscreen', 'codeview']],
  ],
		 onImageUpload: function(files, editor, welEditable) {
            sendFile(files[0],editor,welEditable);
        }	 
		});
	});
</script>
<script type="text/javascript">
function previewImage(event) {
var output = document.getElementById('output');
output.src = URL.createObjectURL(event.target.files[0]);
};
</script>
@endsection