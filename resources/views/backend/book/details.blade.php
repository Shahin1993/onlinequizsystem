 @extends('layouts.admin.master')
 @section('title','Books Details')
 @section('content')
 <div class="page-header">
<h1>
 Books Details 
<small>
<i class="ace-icon fa fa-angle-double-right"></i>
Details
</small>
&nbsp;<a href="/books/create">Add </a>
</h1>
</div><!-- /.page-header -->
 @include('admin.messages.message')
 
<div class="row">
	<div class="col-md-12">
		<p>Category: {{$data->categories->category}} &nbsp; Catlog: {{$data->catelog}}</p>
   <h3>{{$data->title}}</h3>
   {!!$data->description!!}
   <p>Tags: {{$data->tag}}</p>
</div>
</div>

<div class="row">
		@foreach($data->bookimages as $event)
		<div class="col-md-3">
			<img src="{{asset('/images/book_images/'.$event->image)}}" style="height:200px;border: 1px solid gray">
		</div>
		@endforeach
	</div>
 @endsection