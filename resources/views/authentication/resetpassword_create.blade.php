@extends('layouts.authentication.master')
@section('title','reset password')
@section('content')
<div class="container">
  @if(Session::has('success'))
  <div class="alert alert-success">
    {{Session::get('success')}}
  </div>
  @endif
  
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-7" style="background-color: 	 #80bfff;border-radius:2%;">
	<h2>Password Reset form</h2>
  <form class="form-horizontal" action="" method="POST">
    {{csrf_field()}}
   @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    <div class="form-group">
      <label class="control-label col-sm-3" for="password">New Password:</label>
      <div class="col-sm-9">          
        <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-3" for="confirm_password">Confirm Password:</label>
      <div class="col-sm-9">          
        <input type="password" class="form-control" id="confirm_password" placeholder="Enter password again" name="confirm_password">
      </div>
    </div>
      
    <div class="form-group">        
      <div class="col-sm-offset-3 col-sm-9">
        <button type="submit" class="btn btn-success btn-lg">Reset Password</button>
      </div>
    </div>
 </form>
	
	</div>
</div>
</div>
@endsection