@extends('layouts.authentication.master')
@section('title','Sign up')
@section('content')
<div class="container">
  @if(Session::has('success'))
  <div class="alert alert-success">
    {{Session::get('success')}}
  </div>
  @endif
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-7" style="background-color: 	 #80bfff;border-radius:5%;">
	<h2>Registration form</h2>
  <form class="form-horizontal" action="/register" method="POST">
    {{csrf_field()}}
  	<div class="form-group">
      <label class="control-label col-sm-2" for="first_name">First Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="first_name" placeholder="Enter your first name" name="first_name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="last_name">Last Name:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="last_name" placeholder="Enter last name" name="last_name">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="mobile">mobile No:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="mobile" placeholder="Enter your mobile number" name="mobile">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="password">Password:</label>
      <div class="col-sm-10">          
        <input type="password" class="form-control" id="password" placeholder="Enter password" name="password">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="confirm_password">Confirm Password:</label>
      <div class="col-sm-10">          
        <input type="password" class="form-control" id="confirm_password" placeholder="Enter password again" name="confirm_password">
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <div class="checkbox">
          <label><input type="checkbox" name="remember"> Remember me</label>
        </div>
      </div>
    </div>
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-default">Submit</button>
      </div>
    </div>
  </form>
	
	</div>
</div>
</div>
@endsection