@extends('layouts.authentication.master')
@section('title','Reset Password')
@section('content')
<div class="container">
  @if(Session::has('success'))
  <div class="alert alert-success">
    {{Session::get('success')}}
  </div>
  @endif
	<div class="row">
		<div class="col-md-3">
		</div>
		<div class="col-md-7" style="background-color: 	 #80bfff;border-radius:2%;">
	<h2> </h2>
  <form class="form-horizontal" action="/resetpassword" method="POST">
    {{csrf_field()}}
    @if(Session('error'))
  <div class="alert alert-danger">
    {{Session('error')}}
  </div>
  @endif
    <div class="form-group">
      <label class="control-label col-sm-2" for="email">Email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" placeholder="Enter your email for example(infra@gmail.com)" name="email">
      </div>
    </div>
       
  <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success btn-lg">Send</button>
      </div>
    </div>
  </form>
	
	</div>
</div>
</div>
@endsection