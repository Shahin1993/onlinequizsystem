@extends('layouts.frontend.master')
@section('title','Details')

@section('meta')
 
 <meta property="og:url"   content="{{Request::url()}}" />
  <meta property="og:type"   content="website" />
  <meta property="og:title"   content=" {{$data->title}}   " />
  <meta property="og:description"   content="  {{$data->short_description}}   " />
  <meta property="og:image"      content="https://job.jiboneralo.com/frontend/images/jobs22.jpg" />
 
@endsection
 
@section('css')
<style>
  
    table tr td{
        padding:10px;
    }
</style>
@endsection
@section('banner')

 

<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2>{{$data->title}}</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>{{$data->title}}</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')


<section class=" ">
<div class="container">
<div class="row">
    
               
                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_v09t"></div>
            
            
 <div class="col-md-8">
 	<h4>{{$data->title}}</h4><br>
 <span style="text-align:justify;">	{!! $data->description !!} </span>
 
 <br>
 <div class="row">
   @foreach($data->bookimages as $eimage)
	 	<div class="col-md-12" style="margin-bottom:10px;">
	 	<img src="{{asset('/images/book_images/'.$eimage->image)}}"  ><br>
	 </div>
         @endforeach
 
 </div><br>
 

                <!-- Go to www.addthis.com/dashboard to customize your tools -->
                <div class="addthis_inline_share_toolbox_v09t"></div>
            
                <br>
             <!--for seo -->
             
             <!--<h1>  {{$data->tag}} </h1>-->
 </div>
 
 <div class="col-md-4">
     <h4>Related Post</h4>
     <hr>
  	@foreach($books->take(10) as $book)
  	<li><a href="/book-details/{{$book->id}}">{{$book->title}}</a></li> &nbsp;
  	@endforeach
  	
  <h4>Rrecent Post</h4>
     <hr>
  	@foreach($rbooks->take(10) as $rbook)
  	<li><a href="/book-details/{{$rbook->id}}">{{$rbook->title}}</a></li> &nbsp;
  	@endforeach
     
 </div>
     
</div> 



</div>
</section>


@endsection
@section('js')

@endsection