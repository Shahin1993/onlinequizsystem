@extends('layouts.frontend.master')
@section('title','view')
@section('banner')
<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2>{{$catelog}}</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>{{$catelog}}</li>
</ul>
</div>
</div>
</section>
@endsection
@section('css')
<style type="text/css">
	/*.ul-list li:nth-child(2n+1){
     background-color: #b30000;
     color: #ffffff;
	}
	.ul-list li:nth-child(2n+1){
     background-color: #ff4d4d;
     color: #fff;
	}*/
	.ul-list li:hover{
     background-color: #ffffff;
     color: red;
	}
	.ul-list li{
		padding: 10px;
	}
	.ul-list li a{
		font-size:16px;
	}
</style>
@endsection
@section('content')
<section class=" ">
<div class="container">
<div class="row">
	<div class="col-md-12">
  <ul class="ul-list" style="list-style-type: square;">
  	@foreach($books as $book)
  	<li><a href="/book-details/{{$book->id}}">{{$book->title}}</a></li>
  	<p>{{ \Illuminate\Support\Str::words($book->short_description, 50,'....')  }} 
 <a href="/book-details/{{$book->id}}" style="color:red!important;">Read more</a></p>
  	@endforeach
  	
  </ul>
</div>
</div>
</div>
</section>
@endsection