@extends('layouts.frontend.master')
@section('title','about us')
@section('banner')
<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2>About us</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>About us</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')
<section class="features">
<div class="container">
<div class="row">
	<div class="col-md-12">
		<h3>{{$about->name}}</h3><br>
		<span style="text-align:justify;">{!!$about->description!!} </span>
	</div></div>
</div></section>
@endsection