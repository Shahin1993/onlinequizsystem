@extends('layouts.frontend.master')
@section('title','contact')
@section('banner')
<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2>{{$request->keywords}}</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>Search</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')
<section class="features">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			@foreach($books as $book)
			<p style="margin-bottom:6px;">  বিষয়  : {{$book->catelog}}</p>
			<h5><a href="/book-details/{{$book->id}}">  {{$book->title}} </a></h5>
			<p style="margin-top:6px;">{!! substr($book->short_description,0,400)!!} ... <a href="/book-details/{{$book->id}}">  বিস্তারিত </a></p>  <hr>
			@endforeach
		</div>
	</div>
</div>
</section>
@endsection