@extends('layouts.frontend.master')
@section('title','contact')
@section('banner')
 <section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2> Login/Register</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>Login/Register</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')
<div id="content" class="site-content">
<div id="primary" class="content-area">
<main id="main" class="site-main">
<div class="signin-main">
<div class="container">
    @include('admin.messages.message')
<div class="woocommerce">
<div class="woocommerce-login">
    <div class="company-info signin-register">
        <div class="col-md-5 col-md-offset-1 border-dark-left">
            <div class="row">
                <div class="col-md-12">
                    <div class="company-detail bg-dark margin-left">

                        <div class="signin-head">
                            <h2>Sign in &nbsp;  </h2>
                            <span class="underline left"></span>
                        </div> 
        
                        <form class="login" action="/login" method="POST">
                            {{csrf_field()}}
                            <p class="form-row form-row-first input-required">
                                <label>
                                    <span class="first-letter">Student Id</span>  
                                    <span class="second-letter">*</span>
                                </label>
                                <input type="text"  id="username" name="email" class="input-text">
                             </p>
                            <p class="form-row form-row-last input-required">
                                <label>
                                    <span class="first-letter">Password</span>  
                                    <span class="second-letter">*</span>
                                </label>
                                <input type="password" id="password" name="password" class="input-text">
                            </p>
                            <div class="clear"></div>
                            <div class="password-form-row">
                                <p class="form-row input-checkbox">
                                    <input type="checkbox" value="forever" id="rememberme" name="rememberme">
                                    <label class="inline" for="rememberme">Remember me</label>
                                </p>
                                <p class="lost_password">
                                    <a href="#">Lost your Password?</a>
                                </p>
                            </div>
                            <input type="submit" value="Login" name="login" class="button btn btn-default">
                            <div class="clear"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5 border-dark new-user">
            <div class="row">
                <div class="col-md-12">
                    <div class="company-detail new-account bg-light margin-right">
                        <div class="new-user-head">
                            <h2>Create New Account</h2>
                            <span class="underline left"></span>
                            <p></p>
                        </div>
                        <div class="contact-fields">
<form     action="/register" method="POST" >
    {{csrf_field()}}
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="text" placeholder="Name" name="first_name" id="first-name" size="30"   required="" />
</div>
</div>
<div class="col-md-12 col-sm-12">
<div class="form-group">
    <select class="form-control" type="text"  name="department" id="department" size="30"  required="">
        <option value="">Please Select a Department</option>
      <option value="CSE">CSE</option>
      <option value="EEE">EEE</option>
      <option value="CIVIL">CIVIL</option>
      <option value="BBA">BBA</option>
      <option value="ENG">ENGLISH</option>
      <option value="MBA">MBA</option>
      <option value="EMBA">EMBA</option>
    </select>
 
</div>
</div>
 
<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="text" placeholder="Mobile Number" name="mobile" id="phone" size="30"   required="" />
</div>
</div>

<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="text" placeholder="Student ID" name="email"   size="30"  required="" />
</div>
</div>


<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="password" placeholder="Enter your password" name="password" id="password" size="30"   required="" />
</div>
</div>
<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="password" placeholder="confirm password" name="confirm_password" id="confirm_password" size="30" required="" />
</div>
</div>
 
<div class="col-sm-12">
<div class="form-group form-submit">
<input class="btn btn-default"   type="submit"  value="Submit"  />
</div>
</div>
 
</div>
</form> 
</div>       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</main>
</div>
</div>
@endsection