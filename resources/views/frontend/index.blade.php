@extends('layouts.frontend.master')
@section('title','Home')
@section('css')

<style>
.card {
  box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
  transition: 0.3s;
  width: 100%;
}

.card:hover {
  box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}
#signimg{
    position: absolute;right:100px;top:100px;
}
 @media only screen and (max-width: 600px) {
  #signimg{
    right:20px;bottom: 0;top:0;
}
}
 @media only screen and (max-width: 600px) {
  footer{
    padding-top: 50px;
}
}
hr{
     border-top: 1px dashed red;
}
 
</style>
@endsection
@section('content')


<div style="position: relative;margin-top: 100px;">
    <img src="{{asset('images/26-march.jpg')}}"  width="100%;">



  <div   id="signimg">

      <div class="col-md-12 card"   >
         @if(Sentinel::check())
 
         @else
         @include('admin.messages.message')
        <h2 style="color: black;font-weight: bold;">Sign in with Email/Mobile </h2><hr>

          <form class="form-group" action="/login" method="POST">
            {{csrf_field()}}
              <label>Email/Mobile</label>
              <input type="text" name="email" class="form-control">
               <label>Password</label>
              <input type="password" name="password" class="form-control">
              <br>
              <button class="btn btn-success" type="submit">Login</button> &nbsp; &nbsp; &nbsp;<button><a href="/login">Registration</a> </button>

          </form>

           @endif
      </div>
  </div>
 
</div>

 


@endsection