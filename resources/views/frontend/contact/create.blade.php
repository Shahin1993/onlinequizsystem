@extends('layouts.frontend.master')
@section('title','contact')
@section('banner')
<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2>Contact Us</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>Contact</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')

<div id="content" class="site-content">
<div id="primary" class="content-area">
<main id="main" class="site-main">
<div class="contact-main">
<div class="contact-us">
<div class="container">
<div class="contact-location">
	@foreach(App\Contactaddress::all() as $cont)
<div class="flipcard">
<div class="front">
<div class="top-info">
<span><i class="fa fa-map-marker" aria-hidden="true"></i> {{$cont->title}}</span>
</div>
<div class="bottom-info">
<span class="top-arrow"></span>
<ul>
<li>{{$cont->address}} </li>
<li>{{$cont->email}}</li>
<li>{{$cont->mobile}}</li>
</ul>
</div>
</div>
<div class="back">
<div class="bottom-info orange-bg">
<span class="bottom-arrow"></span>
<ul>
<li>{{$cont->address}}</li>
<li>{{$cont->email}}</li>
<li>{{$cont->mobile}}</li>
</ul>
</div>
<div class="top-info dark-bg">
<span><i class="fa fa-map-marker" aria-hidden="true"></i> {{$cont->title}}</span>
</div>                                                
</div>
</div>

@endforeach
<div class="clearfix"></div>
</div>
<div class="row">
<div class="contact-area">
<div class="container">
<div class="col-md-5 col-md-offset-1 border-gray-left">
<div class="row">
<div class="col-md-12">
<div class="contact-map bg-light margin-left">
<div class="company-map" id="map"></div>
</div>
</div>
</div>
</div>
@include('admin.messages.message')
<div class="col-md-5 border-gray-right">
<div class="row">
<div class="col-md-12">
<div class="contact-form bg-light margin-right">
<h2>Send us a message</h2>
<span class="underline left"></span>
<div class="contact-fields">
<form    action="/contacts" method="POST" >
	{{csrf_field()}}
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="text" placeholder="First Name" name="name" id="first-name" size="30"   required="" />
</div>
</div>
 
<div class="col-md-6 col-sm-6">
<div class="form-group">
<input class="form-control" type="email"   placeholder="Email" name="email"   size="30"   required="" />
</div>
</div>
<div class="col-md-6 col-sm-6">
<div class="form-group">
<input class="form-control" type="text" placeholder="Phone Number" name="phone" id="phone" size="30" required="" />
</div>
</div>
<div class="col-md-12 col-sm-12">
<div class="form-group">
<input class="form-control" type="text" placeholder="Subject" name="subject" id="phone" size="30" required="" />
</div>
</div>
<div class="col-sm-12">
<div class="form-group">
<textarea class="form-control" placeholder="Your message" required="" name="details"></textarea>
<div class="clearfix"></div>
</div>
</div>
<div class="col-sm-12">
<div class="form-group form-submit">
<input class="btn btn-default" type="submit"   value="Send Message"  />
</div>
</div>
 
</div>
</form> 
</div>                                                                   
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</main>
</div>
</div>
@endsection