@extends('layouts.frontend.master')
@section('title','catelogs')
@section('banner')
<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2>View Catelogs</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>Catelog</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')
<section class="features">
<div class="container">
<ul>
    @foreach($catelogs as $catelog)
<li class="yellow-hover">
<div class="feature-box">
<i class="yellow"></i>
<h3>{{$catelog->catelog}}</h3>

<a class="yellow" href="/book-catelog/{{$catelog->catelog}}">
    View Details <i class="fa fa-long-arrow-right"></i>
</a>
</div>
</li>
@endforeach
</ul>
</div>
</section>


@endsection