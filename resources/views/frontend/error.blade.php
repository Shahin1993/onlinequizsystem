@extends('layouts.frontend.master')
@section('title','error')
 
 
@section('banner')
<section class="page-banner services-banner">
<div class="container">
<div class="banner-header">
<h2> Warning</h2>
<span class="underline center"></span>
<p class="lead"></p>
</div>
<div class="breadcrumb">
<ul>
<li><a href="/">Home</a></li>
<li>Warning</li>
</ul>
</div>
</div>
</section>
@endsection
@section('content')
 

<section class="  ">
        <div class="">
            <div class="container">
                <div class="row">
<div class="col-md-12">
			<h2 style="color: red;font-weight: bold;">{{$message}}</h2>
		</div>

 </div></div></div></section> 

@endsection