<?php

Route::get('/','FrontpageController@index');
Route::get('/infra','AdminController@index');
Route::group(['middleware'=>'visitor'],function(){
	Route::get('/register','RegistrationController@signup');
Route::post('/register','RegistrationController@store');
Route::get('/login','LoginController@login');
Route::post('/login','LoginController@postLogin');
});
  
  Route::get('/logout','LoginController@logout');
//exam
Route::resource('quizes','Exam\QuizController');

Route::get('/quiz_status/{id}','Exam\QuizController@status');

Route::get('/quize/addquestion/{id}','Exam\QuizController@AddQuestion');

Route::resource('questions','Exam\QuestionController');

Route::get('/quiz','Frontend\QuizController@index');

Route::get('/exam-start/{id}','Frontend\ExamController@exam');

Route::post('/exams','Frontend\ExamController@examPost');
Route::get('/MyExamResult','Frontend\ExamController@examResult');
Route::get('/profile','Frontend\ExamController@examResult');

Route::get('/exmResults','Exam\ResultController@viewResult');

Route::get('/MyExamDetails/{id}','Frontend\ExamController@ResultDetails');
  